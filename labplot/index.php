<?php
  $site_root = "../";
  $page_title = 'LabPlot';
  
  include ( "header.inc" );
?>

<?php
  include("labplot.inc");
  $appinfo->showIconAndCopyright();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">Latest news</a> 
]
</div>

<center>
<a href="pics/labplot_sm.png"><img border="0" width="330" height="200" src="pics/labplot_sm.png"></a>
</center>

<h3><a name="description">Description</a></h3>

<p>
LabPlot is a KDE-application for interactive graphing and analysis of 
scientific data. LabPlot provides an easy way to create, manage and edit 
plots.
</p>

<p>
For a brief history about the beginning of LabPlot2 read the interview on
<a href="https://dot.kde.org/2009/10/16/labplot-and-scidavis-collaborate-future-free-scientific-plotting">dot.kde.org</a>.
See also a recent blog that demonstrate <a href="http://www.asinen.org/2013/10/labplot-2-and-the-state-of-free-data-analysis/">a couple of LabPlot2′s features</a> 
</p>

<p>Features:
<ul>
<li>Project-based management of data</li>
<li>Project-explorer for management and organization of created objects in different folders and sub-folders</li>
<li>Spreadsheet with basic functionality for manual data entry or for generation of uniform and non-uniform random numbers</li>
<li>Import of external ASCII-data into the project for further editing and visualization</li>
<li>Export of spreadsheet to an ASCII-file</li>
<li>Worksheet as the main parent object for plots, labels etc., supports different layouts and zooming functions</li>
<li>Export of worksheet to different formats (pdf, eps, png and svg)</li>
<li>Great variety of editing capabilities for properties of worksheet and its objects</li>
<li>Cartesian plots, created either from imported or manually created data sets or via mathematical equation</li>
<li>Definition of mathematical formulas is supported by syntax-highlighting and completion and by the list of thematically grouped mathematical and physical constants and functions.</li>
<li>Analysis of plotted data is supported by many zooming and navigation features.</li>
<li>Linear and non-linear fits to data, several fit-models are predefined and custom models with arbitrary number of parameters can be provided</li>
</ul>
</p>


<?php
//    kde_general_news("./news.rdf", 10, true);
?>

<br />
<br />

<hr width="30%" align="center" />
<p>
Author: Alexander Semke<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
    include "footer.inc";
?>
