<?php
  $site_root = "../";
  $page_title = 'KLettres';
  
  include ( "header.inc" );
?>

<?php
  include("klettres.inc");
  $appinfo->showIconAndCopyright();
?>

<br />
<div id="quicklinks">
[
  <a href="#features">Features</a> |
  <a href="#news">News</a>
]
</div>

<p>KLettres aims to help to learn the alphabet and then to read some syllables in different languages.<br />
It is meant to help learning the very first sounds of a new language, for children or for adults.<br />
</p>

<p>
Currently 25 languages are available: <b>Arabic, Czech, Brazilian Portuguese, Danish, Dutch, British English, English,
English Phonix, French, German, Hebrew, Hungarian, Italian, Kannada, Hebrew, Hindi Romanized,
Low Saxon, Luganda, Malayalam, Norwegian Bokmål, Punjabi, Spanish, Slovak, Ukrainian</b> and <b>Telugu</b>, you can choose them using the <i>Languages</i> menu.
<br />
A toolbar with the special characters per language is provided if you don't have the correct country keyboard or the keyboard layout to be able to display correctly the accented letters.
</p>

<center>
<a href="pics/klettres_2008_01.png"><img border="0" width="" height="" src="pics/klettres_2008_01_330.png"></a>
</center>

<h3><a name="features">Features</a></h3>
KLettres has four levels which can be changed via a <i>Levels</i> drop down box or the Level menu:
<ul>
<li><b>Level 1</b>: the letter is displayed and the user hears it.</li>
<li><b>Level 2</b>: the letter is not displayed, the user only hears it.</li>
<li><b>Level 3</b>: the syllable is displayed and the user hears it.</li>
<li><b>Level 4</b>: the syllable is not displayed, the user only hears it.</li>
</ul>
<p>You can also choose the mode (kid or grown-up) via the colored buttons on the toolbar and KLettres will keep your settings and restore them the next time you play.</p>
<p><img src="pics/get-hot-new-stuff.png" width="22" height="22" alt="New Stuff" style="float: left right:3px" /> You can use the game to learn the alphabet in other languages.
There are <b>twenty-five languages</b> available at the moment (please see list above). The program will detect which languages are present and enable them. You will also be able to easily download other languages via the Get New Stuff dialog (3 clicks and your data will be installed).</p>
<p><img src="pics/configure.png" width="22" height="22" alt="Configure" style="float: left right:3px" /> The <i>Settings</i> menu allows you the usual KDE settings. You can also change the font that displays the letter/syllable as in some distributions, the default big font is quite ugly!
Furthermore, languages as Czech or Slovak might need a font as Arial for example to display the accented characters properly.</p>
<p><img src="pics/preferences-desktop-locale.png" width="22" height="22" alt="Configure" style="float: left right:3px" /> If you want to contribute for your own language, please have a look <a href="add_language.php">here</a>. Thank you in advance!</p>

<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
