<?php
  $site_root = "../";
  $page_title = 'KLettres - How to add a new language';
  
  include ( "header.inc" );
  ?>
 <h3>HowTo add a new language to KLettres</h3>

<p>Here is a an outline of what to do if you want to add your own language to KLettres. Please keep in mind that KLettres is meant to help learning the alphabet and the more useful syllables in your language. Maybe some letters of the alphabet are never pronounced so you can remove those.</p>
<p>
You will need 2 sets of sounds, one for the alphabet and one for the syllables, one file with the data, and one file with the special characters in your language, if there are any.
</p>

<h4>RECORDING THE SOUNDS</h4>
<ul>
<li>
Put each sound in a file, wav, ogg or mp3. ogg is better as it's free (versus mp3)<br />
the alphabet sounds can be stored as a-0.ogg, a-1.ogg, etc. or a.ogg, b.ogg etc.<br />
the syllables sounds can be stored as ad-0.ogg, ad-1.ogg and so on or ba.ogg, bi.ogg, etc..,<br />
but use only lower ascii characters for the file names.
</li>
<li>
Each sound needs to be 1.5 to 2 seconds. If shorter, it will not play. Please add blank if necessary.
</li>
<li>
Place all the alphabet sounds in a folder named <i>alpha</i> and all syllables sounds in a folder named <i>syllab</i>
</li>
<li>
Create a text file <i>sounds.xml</i>, preferably using Kate as editor or another editor capable of different encoding. In this file, write all the alphabet sounds and syllable sounds you recorded, like this example for the czech language:<br />
&lt;klettres&gt;<br />
&lt;language code="cs"&gt;<br />
    &lt;menuitem&gt;
      &lt;label&gt;&amp;Czech&lt;/label&gt;<br />
    &lt;/menuitem&gt;<br />
    &lt;alphabet&gt;<br />
        &lt;sound name="A" file="cs/alpha/a.ogg" /&gt;<br />
       ...<br />
    &lt;/alphabet&gt;<br />
    &lt;syllables&gt;<br />
        &lt;sound name="BA" file="cs/syllab/ba.ogg" /&gt;<br />
       ...<br />
    &lt;/syllables&gt;<br />
  &lt;/language&gt;<br />
&lt;/klettres&gt;<br />
Replace "cs" with the two letter code and "Czech" with the name of your language. Write the sound names with the special characters of your language in upper case in utf8.<br />
Create a text file <i>cs.txt</i> (replace "cs" with the two letter code of your language) with each special character in your language in upper case on one line.<br />
And save both text files with <b>utf8</b> encoding (upper right dropbox in kate allows that)<br />
</li>
<li>
tar the 2 sounds directories plus the 2 text files and please send them to <a href="mailto:annemarie.mahfouf AT free DOT fr">that address</a>
</li>
<li>
I will then place the sounds in the corresponding i18n module and also on the KLettres website so they will be available via File -> Get A New Language in KLettres.
</li>
</ul>
Good luck and thanks a lot!<br />



  <p>
 Author: Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>
