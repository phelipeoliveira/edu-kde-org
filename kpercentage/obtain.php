<?php
  $page_title = "How to obtain KPercentage";
  
  include( "header.inc" );
?>

<br />
<div id="quicklinks"> 
	[ 
	<a href="#binpacks">Binary Packages</a> 
	|
	<a href="#tarballs">Source Tarballs</a> 
	|
	<a href="#svn">SVN</a> 
	]
</div>
<br />

<?php
  show_obtain_instructions( "KPercentage" );
?>

<hr width="30%" align="center" />
<p>Author: Matthias Me&szlig;mer<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include("footer.inc");
?>

