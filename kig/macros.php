<?php
  $site_root = "../";
  $page_title = "Kig Macro Repository";


  include( "header.inc" );
?>

<br />
<h3>Macro Repository</h3>

<p>Since Kig can import and use third-part macros as built-in objects, it can
  have new objects, being more and more powerful.</p>

<p>This page is meant as a repository for macros you can use in Kig.<br />
  If you have an interesting or nice macro you want to share with the world,
  don't hesitate to contact the Kig authors.</p>

<p>To <b>download a macro</b>, just click on the correspondent Kig icon.</p>

<p>If you don't know how to <b>import a macro</b>, please read the
  <a href="faq.php" title="Frequently Asked Questions about Kig">FAQ</a>.</p>

<table summary="Kig Macros">
<tbody>
<tr>
  <th># of<br />mac.</th>
  <th>Name</th>
  <th>Description</th>
  <th>Author</th>
  <th>License</th>
  <th>File</th>
</tr>
<tr>
  <td>1</td>
  <td>cirkel</td>
  <td>A circle BTP constructed using the circle BCP</td>
  <td>Dominique Devriese</td>
  <td>GPL</td>
  <td><a href="downloads/macros/circleBTP_by_circleBCP.kigt" title="circleBTP_by_circleBCP.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="circleBTP_by_circleBCP.kigt" />
      </a></td>
</tr>
<tr>
  <td>1</td>
  <td>ConicByLocus</td>
  <td>A conic constructed as a locus</td>
  <td>Dominique Devriese</td>
  <td>GPL</td>
  <td><a href="downloads/macros/conic_by_locus.kigt" title="conic_by_locus.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="conic_by_locus.kigt" />
      </a></td>
</tr>
<tr>
  <td>1</td>
  <td>ConicCenter</td>
  <td>The center of a conic</td>
  <td>Dominique Devriese</td>
  <td>GPL</td>
  <td><a href="downloads/macros/ConicCenter.kigt" title="ConicCenter.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="ConicCenter.kigt" />
      </a></td>
</tr>
<tr>
  <td>1</td>
  <td>LineConicMainAxis</td>
  <td>The main axis of a conic</td>
  <td>Dominique Devriese</td>
  <td>GPL</td>
  <td><a href="downloads/macros/LineConicMainAxis.kigt" title="LineConicMainAxis.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="LineConicMainAxis.kigt" />
      </a></td>
</tr>
<tr>
  <td>1</td>
  <td>LineConicSecondAxis</td>
  <td>The secondary axis of a conic</td>
  <td>Dominique Devriese</td>
  <td>GPL</td>
  <td><a href="downloads/macros/LineConicSecondAxis.kigt" title="LineConicSecondAxis.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="LineConicSecondAxis.kigt" />
      </a></td>
</tr>
<tr>
  <td>1</td>
  <td>Translatie</td>
  <td>Mirror a point using the translation</td>
  <td>Dominique Devriese</td>
  <td>GPL</td>
  <td><a href="downloads/macros/mirrorpoint_by_translationpoint.kigt" title="mirrorpoint_by_translationpoint.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="mirrorpoint_by_translationpoint.kigt" />
      </a></td>
</tr>
<tr>
  <td>6</td>
  <td>
    <ol type="1">
      <li>Baricenter</li>
      <li>Circumcenter</li>
      <li>Gauss Segment</li>
      <li>Incenter</li>
      <li>Inscribed circle</li>
      <li>Ortocenter</li>
    </ol>
  </td>
  <td>
    <ol type="1">
      <li>Baricenter of a triangle, given the vertices</li>
      <li>Circumcenter of a triangle, given the vertices</li>
      <li>Gauss Segment of a triangle, given the vertices</li>
      <li>Incenter of a triangle, given the vertices</li>
      <li>Inscribed circle on a triangle, given the vertices</li>
      <li>Ortocenter of a triangle, given the vertices</li>
    </ol>
  </td>
  <td>Noel Torres</td>
  <td>Public Domain</td>
  <td><a href="downloads/macros/triangle_centers.kigt" title="triangle_centers.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="triangle_centers.kigt" />
      </a></td>
</tr>
<tr>
  <td>2</td>
  <td>star5 - star5b</td>
  <td>Two different ways to construct a star.</td>
  <td>Maurizio Paolini</td>
  <td>GPL</td>
  <td><a href="downloads/macros/starswith5points.kigt" title="starswith5points.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="starswith5points.kigt" />
      </a></td>
</tr>
<tr>
  <td>4</td>
  <td>
    <ol type="1">
      <li>TropicalLine1pt</li>
      <li>TropicalLine2pt</li>
      <li>TropicalLineLineIntersection</li>
      <li>TropicalConic</li>
    </ol>
  </td>
  <td>
    <ol type="1">
      <li>a tropical line constructed using its centre point</li>
      <li>The stable tropical line passing through two points</li>
      <li>The stable intersection of two tropical lines, to select a tropical
          line, select its centre point</li>
      <li>Stable Conic Passing through 5 points</li>
    </ol>
    <img border="0" src="pics/source_py.png" alt="Python script" align="right" hspace="5" />
    These macros require a Kig version with the Python script.
  </td>
  <td>Luis Felipe Tabera<br/>
      &lt;<a href="mailto:lftabera AT yahoo DOT es">lftabera AT yahoo DOT es</a>&gt;</td>
  <td>GPL</td>
  <td><a href="downloads/macros/tropical_geometry.kigt" title="tropical_geometry.kigt">
      <img border="0" src="../images/icons/kig_32.png" alt="tropical_geometry.kigt" />
      </a></td>
</tr>
</tbody>
</table>

<?php include( "footer.inc" ); ?>
