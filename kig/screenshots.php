<?php
  $site_root = "../";
  $page_title = 'Kig Screenshots';

  include( "header.inc" );
  include( "../site_includes/class_edugallery.inc" );

  $gal = new EduGallery( "Kig Screenshots" );

  $gal->addImage( "pics/kig-snap-exporters.thumb.png", "pics/kig-snap-exporters.png", 200, 149, "[Screenshot]", "Kig exporters", "The formats you can export your kig constructions to..." );
  $gal->addImage( "pics/kig-snap-select-and-names.thumb.png", "pics/kig-snap-select-and-names.png", 200, 149, "[Screenshot]", "Selection box &amp; names", "A selection box and some objects with a label showing their names" );
  $gal->addImage( "pics/kig-snap-polygons.thumb.png", "pics/kig-snap-polygons.png", 200, 149, "[Screenshot]", "Polygons", "Kig dealing with polygons, with the construction of a regular polygon" );

  $gal->startNewRow();

  $gal->addImage( "pics/kig-snap-sine-curve.thumb.png", "pics/kig-snap-sine-curve.png", 200, 149, "[Screenshot]", "A sine-curve!", "A sine-curve in Kig, created using a combination of the new Python Scripting feature, and the support for loci" );
  $gal->addImage( "pics/kig-snap-christophe-playing-1.thumb.png", "pics/kig-snap-christophe-playing-1.png", 115, 113, "[Screenshot]", "Kig","My brother playing around :)" );
  $gal->addImage( "pics/kig-snap-christophe-playing-2.thumb.png", "pics/kig-snap-christophe-playing-2.png", 206, 144, "[Screenshot]", "Kig", "My brother still playing around.." );

  $gal->startNewRow();

  $gal->addImage( "pics/kig-snap-constructing-radical-line.thumb.png", "pics/kig-snap-constructing-radical-line.png", 129, 100, "[Screenshot]", "Radical line", "One of the cool new Conic objects by Maurizio Paolini: the radical line of two conics.." );
  $gal->show();

  include( "footer.inc" );
?>
