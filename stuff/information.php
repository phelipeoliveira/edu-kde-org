<?php
  $page_title = "Information";
  $site_root = "../";
  $site_search = false;
  

  include( "header.inc" );
?>

<h3>Online Presentation of KDE-Edu applications in KDE 4.0</h3>
<p>
Please see <a href="../tour_kde4.0/index.php">the online KDE 4.0 Tour</a> and learn about our software!
</p>

<h3>How did it start?</h3>
<p>The KDE-Edu project was started in July 2001. The goal of the project is to develop Free Educational Software (GPL licence) within the KDE environment. This software is mainly aimed to schools, to students and to parents at home as well as to adults willing to extend their knowledge. <br />
A new KDE module was created, the kdeedu module. The applications in that module are fully KDE compatible (use of KMainWindow, XML GUI, ...) and get a proper documentation handbook in docbook format. Thus, all applications interfaces and documentation are translated in more than 65 languages, via the l10n KDE teams.</p>
<p>
The kdeedu module joined the KDE 3.0 official release.
</p>

<h3>I want to join and contribute!</h3>
<p>
The <a href="get_involved.php">Get Involved webpage</a> give you some ideas on how you can contribute and everyone is welcome and can find something to do in our project.
</p>

<h3>Contact</h3>
<p>
Please see the <a href="mailinglists.php">Contacts webpage</a>.
</p>

<br />
<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>













