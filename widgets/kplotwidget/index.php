<?php
	$page_title = "KPlotWidget";
	include( "header.inc" );

	$appinfo = new EduAppInfo( "KPlotWidget" );
	$appinfo->setLicense("gpl");
	$appinfo->setCopyright( 2003, "Jason Harris" );
	$appinfo->addAuthor( "Jason Harris", "kstars AT 30doradus DOT org" );
	$appinfo->showIconAndCopyright();
?>

<h3><a name="description">Description</a></h3>

<p>KPlotWidget is a generic data plotting widget.</p>

<p>The basic idea behind KPlotWidget is that
you don't have to worry about any transformation from your data's
natural units to screen pixel coordinates; this is handled internally
by the widget.</p>

<p>Data to be plotted are represented by one or more instances of
KPlotObject.  KPlotObject contains a list of QPointFs to be plotted
(again, in the data's natural units), as well as information about how
the data are to be rendered in the plot (i.e., as separate points or
connected by lines?  With what color and point style? etc).  See
KPlotObject for more information.</p>

<p>KPlotWidget is part of the libkdeedu library. It is allready used in <a href="<?php echo $site_root; ?>/ktouch">KTouch</a>, <a href="<?php echo $site_root; ?>/kstars">KStars</a>, <a href="<?php echo $site_root; ?>/kalzium">Kalzium</a>, and some others.</p>

<?php
  $appinfo->showPeople();
  $appinfo->showLicence();
?>

<h3><a name="#links">Read further...</a></h3>

<ul>
  <li><a href="http://developer.kde.org/documentation/library/cvs-api/kdeedu-apidocs/libkdeedu/html/classKPlotWidget.html">KPlotWidget class reference</a></li>
  <li><a href="screenies.php">Screenshots</a> of KPlotWidget in action</a></li>
</ul>

<hr width="30%" align="center" />

<p>
Author: Matthias Me&szlig;mer<br />
Text mostly taken from the source code comments.<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
