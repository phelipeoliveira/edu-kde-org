 <?php
  $appinfo = new AppInfo( "Rocs" );
  $appinfo->setIcon( "../images/icons/rocs_32.png", "32", "32");
  $appinfo->setVersion( "1.7" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2009","Rocs Developers" );
  $appinfo->addAuthor( "Tomaz Canabrava", "tomaz.canabrava AT gmail DOT com" );
  $appinfo->addAuthor( "Wagner Reck", "wagner.reck AT gmail DOT com" );
  $appinfo->addAuthor( "Andreas Cord-Landwehr", "cordlandwehr AT gmail DOT com" );
  $appinfo->addContributor("Ugo Sangiori");
?>
