<?php

// Collect news from KDE-Edu applications

function kde_edu_app_news( $folder, $app_name, $items, &$dates, &$titles, &$apps, &$index )
{
  $news = new RDF();
  $rdf_pieces = $news->openRDF( $folder . "/news.rdf" );

  if(!$items)
  {
     $items = 5; // default
  }
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items)
  {
     $rdf_items = $items;
  }

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    if ($rdf_items < $items)
    {
      $rdf_items = $rdf_items - 1;
    }

    for($x=0;$x<$rdf_items;$x++)
    {
      ereg("<title>(.*)</title>", $rdf_pieces[$x], $title);
      ereg("<date>(.*)</date>", $rdf_pieces[$x], $date);
      $printDate = $date[1];
      
      $index++;
      $dates[ $index ] = strtotime( $date[ 1 ] );
      $titles[ $index ] = "<a href=\"" . $folder . "/news.php#item" . ereg_replace( "[^a-zA-Z0-9]", "", $title[ 1 ] ) . "\">" . $title[ 1 ] . "</a>";
      $apps[ $index ] = "<a href=\"$folder\">$app_name</a>";
    }
  }
}

// Sort and output top news

function kde_edu_news()
{
  global $site_locale;
  startTranslation($site_locale);
  
  print "<h2><a name=\"news\">" . i18n_var("Latest News") . "</a></h2>\n";
  print "<table style=\"width: 100%;\" class=\"table_box\" cellpadding=\"6\">\n";
  print "<tr>\n<th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th>\n<th>Application</th>\n</tr>\n";

  $index = 0;
  $dates = array();
  $titles = array();
  $apps = array();

  $num_main_rdf = 8;
  $num_app_rdf = 4;

  kde_edu_app_news ( ".", "", $num_main_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "blinken", "blinKen", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "kalzium", "Kalzium", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "kanagram", "Kanagram", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "khangman", "KHangman", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "klettres", "KLettres", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "kstars", "KStars", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "ktouch", "KTouch", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "kturtle", "KTurtle", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "kverbos", "KVerbos", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "kwordquiz", "KWordQuiz", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "marble", "Marble", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "parley", "Parley", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "step", "Step", $num_app_rdf, $dates, $titles, $apps, $index );
  kde_edu_app_news ( "cantor", "Cantor", $num_app_rdf, $dates, $titles, $apps, $index );
  array_multisort( $dates, SORT_DESC, $titles, $apps );

  $number = min( count( $dates ), 10 );
  $alternate =false;
  
  for( $index=0; $index<$number; $index++ )
  {
    $alternate = !$alternate;
    if ($alternate)
    {
      $color = "newsbox1";
    }
    else
    {
      $color = "newsbox2";
    }
    $printDate = date( "jS F Y", $dates[ $index ] );
    print "<tr>\n"
         ."<td valign=\"top\" class=\"$color\">" . (($printDate == $prevDate) ? "&nbsp;" : "<b>$printDate</b>") . "</td>\n"
	 ."<td valign=\"top\" class=\"$color\">" . $titles[ $index ] . "</td>\n"
	 ."<td valign=\"top\" class=\"$color\">" . $apps[ $index ] . "</td>\n"
	 ."</tr>\n";
    $prevDate = $printDate;
  }
  print "</table>\n";
} 

?>