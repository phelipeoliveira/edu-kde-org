<?php

/**
* OCS Lib
*
* @author Frank Karlitschek 
* @copyright 2010 Frank Karlitschek karlitschek@kde.org 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either 
* version 3 of the License, or any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*  
* You should have received a copy of the GNU Lesser General Public 
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
* 
*/



class OCS {

  static function importall(){

    $categories =array(
      '3'=>array('name'=>'Education','ids'=>'242'),
      );
   
    foreach($categories as $key=>$value) {
      OCS::import($key,$value);
    } 

  }

  static function import($category,$ocscategories){
    $url='http://api.opendesktop.org/v1/content/data?categories='.$ocscategories['ids'].'&sortmode=new&page=0&pagesize=10';

    $xml=file_get_contents($url);
    $data=simplexml_load_string($xml);

    // remove old stuff
    $request=DB::query('delete from ocs where category="'.addslashes($category).'"');
    DB::free_result($request);


    $tmp=$data->data->content;
    for($i = 0; $i < count($tmp); $i++) {

      $request=DB::query('insert into ocs (category,name,type,user,url,preview,timestamp,description) values("'.$category.'","'.addslashes($tmp[$i]->name).'", "'.addslashes($tmp[$i]->typeid).'","'.addslashes($tmp[$i]->personid).'","'.addslashes($tmp[$i]->detailpage).'", "'.addslashes($tmp[$i]->smallpreviewpic1).'","'.addslashes(strtotime($tmp[$i]->changed)).'","'.addslashes($tmp[$i]->description).'"  ) ');
      DB::free_result($request);

    } 





  }




  static function show($category,$count){
    global $kde_contributors;

    $dateformat="M j G:i";

    $sql='select name,type,user,url,preview,timestamp,description from ocs where category='.$category.' order by timestamp desc limit '.$count;
    $request=DB::query($sql);
    $num=DB::numrows($request);
    echo('<div class="table-wrapper"><table class="ocs">');
    for($i = 0; $i < $num; $i++) {
      echo('<tr><td class="ocs-applicationshot">');
      $data=DB::fetch_assoc($request);
      if(isset($data['preview']) and !empty($data['preview'])) echo('<a href="'.$data['url'].'"><img src="'.$data['preview'].'" alt="'.$data['name'].'" title="'.$data['name'].'" /></a>');

      echo('</td><td class="ocs-latestapp">');
      echo('<span class="ocs-title"><a href="'.$data['url'].'">'.$data['name'].'</a></span><br />');
      if(!empty($data['description'])) echo(''.substr($data['description'],0,200).' ...');
      echo('</td></tr>');
    }
    echo('</table></div>');
    DB::free_result($request);

  }





}


?>
