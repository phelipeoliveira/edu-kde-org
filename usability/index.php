<?php
  $site_root = "../";
  $page_title = 'Usability Reports';
  
  include ( "header.inc" );
?>

 <h2>Year 2005</h2>

<table cellspacing="0" cellpadding="3" border="1">
<tr>
    <th>Application</th>
    <th>Link to report</th>
    <th>Date of report</th>
    <th>Target - Maintainer</th>
  </tr>
  <tr>
    <td><b>KBruch</b></td>
    <td><a href="http://obso1337.org/hci/kbruch/">http://obso1337.org/hci/kbruch/</a></td>
    <td>22 October2005</td>
    <td>KDE4 - <a href="mailto:seb_stein@gmx.de">Sebastian Stein</a><br />
Follow up can be done here: <a href="http://openusability.org/forum/message.php?msg_id=1374">http://openusability.org/forum/message.php?msg_id=1374</a></td>
  </tr>
</table>

<br />
<br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
