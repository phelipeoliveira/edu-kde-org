<?php
  $site_root = "../";
  $page_title = 'KTouch';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KTouch" );
  $appinfo->setIcon( "../images/icons/ktouch_32.png", "32", "32" );
  $appinfo->setVersion( "1.7.1" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2000-2009", "Haavard Froeiland and  Andreas Nicolai" );
  $appinfo->addAuthor( "Haavard Froeiland", "haavard -AT- users DOT sourceforge.net " );
  $appinfo->addContributor("Andreas Nicolai", "ghorwin -AT- users DOT sourceforge.net  ", "Current maintainer");
  $appinfo->addContributor("Anne-Marie Mahfouf", "annma -AT- kde DOT org", "Port to KConfig Compiler, coding updates, bug fixes");
  $appinfo->addContributor( "David Vignoni", "david80v -AT- tin DOT it", "new SVG icons" );
  $appinfo->addContributor( "Marc Heyvaert", "marc_heyvaert -AT- yahoo DOT com", "Data files, bug fixes" );
  $appinfo->addContributor( "Christian Spener", "", "Data files, bug fixes" );
  $appinfo->show();
?>
<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">Latest news</a> 
]
</div>

<center>
<?php
  $gallery = new EduGallery("KTouch - Screenshot");
  $gallery->addImage("pics/ktouch_sm.png", "pics/ktouch.png", 330, 215,  "[Screenshot]", "", "KTouch");
  $gallery->show();
 ?>
 </center>

<h3><a name="description">Description</a></h3>

<p>KTouch is a program for learning touch typing. KTouch offers you an easy way to learn to type quickly and correctly. Every finger has its place on the keyboard with associated keys to press. Starting with only a few keys to remember you will advance through different training levels where additional keys are introduced. Because you no longer need to search for the keys on the keyboard you will be able to type quickly and accurately.</p>

<p>Even if you type quickly already, learning 10-finger touch-typing will allow you to type much faster, and you will definitely be using more than just two fingers.
</p>

<?php
  kde_general_news("./news.rdf", 10, true);
 ?>
<br />
<hr width="30%" align="center" />
Author: Anne-Marie Mahfouf<br />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

