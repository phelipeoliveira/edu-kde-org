<?php
  $site_root = "../";
  $page_title = 'Cantor';
  
  include ( "header.inc" );
?>

<?php
  include("cantor.inc");
  $appinfo->showIconAndCopyright();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a>
  <a href="#features">Features</a>
  <a href="#news">Latest news</a> 
]
</div>

<?php
  // for now only use the auto-generated information
  print '<div id="sidebar">';
  print printSidebar($app, 'all', false);
  print '</div>';
  printPage($app);
?>

<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<br />

<hr width="30%" align="center" />

<?php
  include "footer.inc";
?>
