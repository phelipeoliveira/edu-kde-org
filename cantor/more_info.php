<?php
  $site_root = "../";
  $page_title = 'About Cantor';
  
  include ( "header.inc" );
?>

<?php
  include("cantor.inc");
  $appinfo->show();
?>

<br />
<br />

<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php include("footer.inc"); ?>