<?php
  $site_root = "../";
  $page_title = 'KStars I18N Support';
  
  include ( "header.inc" );
?>

<h2>Into what languages has KStars been translated?</h2>

<p>
Thanks to the tireless efforts of the many 
<a href="http://i18n.kde.org">KDE translation teams</a>, the GUI strings
and documentation for KStars may be available in your native language.
Here is a table listing the language teams which have translated at 
least 50% of the strings in KStars for KDE-3.5:
</p>

<h3>GUI Strings:</h3>
<table cellpadding="20" cellspacing="10">
<tr>
<td valign="top" bgcolor="#00AAFF">
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ar">Arabic</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ar/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=eu">Basque</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/eu/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=bn">Bengali</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/bn/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=bs">Bosnian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/bs/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=pt_BR">Brazilian Portuguese</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/pt_BR/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=br">Breton</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/br/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=en_GB">British English</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/en_GB/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=bg">Bulgarian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/bg/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ca">Catalan</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ca/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=zh_CN">Chinese Simplified</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/zh_CN/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=hr">Croatian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/hr/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=cs">Czech</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/cs/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=da">Danish</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/da/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=nl">Dutch</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/nl/messages/kdeedu/kstars.po">get</a>)<br />
</td>
<td valign="top" bgcolor="#00AAFF">
<a href="http://i18n.kde.org/teams/infos.php?teamcode=eo">Esperanto</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/eo/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=et">Estonian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/et/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=fi">Finnish</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/fi/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=fr">French</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/fr/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ka">Georgian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ka/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=de">German</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/de/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=el">Greek</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/el/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=hi">Hindi</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/hi/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=hu">Hungarian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/hu/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=is">Icelandic</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/is/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ga">Irish Gaelic</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ga/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=it">Italian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/it/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ja">Japanese</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ja/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=mk">Macedonian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/mk/messages/kdeedu/kstars.po">get</a>)<br />
</td>
<td valign="top" bgcolor="#00AAFF">
<a href="http://i18n.kde.org/teams/infos.php?teamcode=nb">Norwegian Bokmål</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/nb/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=pl">Polish</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/pl/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=pt">Portuguese</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/pt/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ru">Russian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ru/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=sr">Serbian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/sr/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=sk">Slovak</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/sk/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=sl">Slovenian</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/sl/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=es">Spanish</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/es/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=sv">Swedish</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/sv/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=tg">Tajik</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/tg/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ta">Tamil</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/ta/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=tr">Turkish</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/tr/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=cy">Welsh</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/cy/messages/kdeedu/kstars.po">get</a>)<br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=xh">Xhosa</a>
(<a href="http://websvn.kde.org/*checkout*/branches/stable/l10n/xh/messages/kdeedu/kstars.po">get</a>)<br />
</td>
</tr>
</table>

<h3>Documentation:</h3>
<table cellpadding="20" cellspacing="10">
<tr>
<td valign="top" bgcolor="#00AAFF">
<a href="http://i18n.kde.org/teams/infos.php?teamcode=pt_BR">Brazilian Portuguese</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=en_GB">British English</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ca">Catalan</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=da">Danish</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=nl">Dutch</a><br />
</td>
<td valign="top" bgcolor="#00AAFF">
<a href="http://i18n.kde.org/teams/infos.php?teamcode=et">Estonian</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=fr">French</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=de">German</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=it">Italian</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=rw">Kinyarwanda</a><br />
</td>
<td valign="top" bgcolor="#00AAFF">
<a href="http://i18n.kde.org/teams/infos.php?teamcode=pl">Polish</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=pt">Portuguese</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=ru">Russian</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=es">Spanish</a><br />
<a href="http://i18n.kde.org/teams/infos.php?teamcode=sv">Swedish</a><br />
</td>
</tr>
</table>

<p>
If your language is not listed, or if the translation is incomplete, 
please consider joining one of the <a href="http://i18n.kde.org">KDE 
translation teams</a>, and help bring KStars and the rest of KDE to a 
wider audience. 
</p>

<hr />
<p>
Jason Harris<br />
<a href="mailto:kstars AT 30doradus DOT org">kstars AT 30doradus DOT org</a>
</p>

<?php
  include "footer.inc";
?>
