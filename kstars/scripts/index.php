<?php
  $site_root = "../..";
  $page_title = 'KStars Scripts Archive';
  
  include ( "header.inc" );
?>

<p>
Using KDE's powerful DCOP system, KStars allows you to 
orchestrate complex behaviors into shell scripts.  It 
even provides a GUI 
<a href="http://docs.kde.org/en/3.3/kdeedu/kstars/tool-scriptbuilder.html">ScriptBuilder 
Tool</a> to make creating scripts easy and fun.
</p>
<p>
On this page, we will collect KStars scripts contributed by 
users, so that everyone can enjoy them.  If you'd like to
contribute a script, please send it to 
<a href="mailto:kstars AT 30doradus DOT org">kstars AT 30doradus 
DOT org</a>.
</p>

<h3>The Scripts</h3>
<table cellpadding="6">
<tr>
<td><b>Script name</b></td>
<td><b>Description</b></td>
<td><b>Author</b></td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Lunar_Phases.kstars">Lunar Phases</a></td>
<td bgcolor="#DDDDDD">Track the Moon as it goes through a cycle 
of its phases.
<a href="Lunar_Phases.txt">(more...)</a>
</td>
<td>Jason Harris</td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Celestial_Pole.kstars">Celestial Pole</a></td>
<td bgcolor="#DDDDDD">Watch the sky rotating about the celestial pole.
<a href="Celestial_Pole.txt">(more...)</a></td>
<td>Jason Harris</td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Move_North.kstars">Move North</a></td>
<td bgcolor="#DDDDDD">Learn how your latitude affects the appearance 
of the sky.
<a href="Move_North.txt">(more...)</a></td>
<td>Jason Harris</td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Seasons_Noon.kstars">Seasons at Noon</a></td>
<td bgcolor="#DDDDDD">Watch the appearance of the Sun at Noon over 
the course of a year.  
<a href="Seasons_Noon.txt">(more...)</a></td>
<td>Jason Harris</td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Jovian_Moons.kstars">Jovian Moons</a></td>
<td bgcolor="#DDDDDD">Watch Jupiter's Moons as they orbit Jupiter 
<a href="Jovian_Moons.txt">(more...)</a></td>
<td>Jason Harris</td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Mars_Retrograde.kstars">Mars Retrograde</a></td>
<td bgcolor="#DDDDDD">Watch Mars as it executes its 
<a href="http://docs.kde.org/en/3.3/kdeedu/kstars/ai-retrograde.html">retrograde motion</a> 
in the Summer of 2005. 
<a href="Mars_Retrograde.txt">(more...)</a></td>
<td>Jason Harris</td>
</tr>
<tr>
<td bgcolor="#FFFFAA"><a href="Halleys_Comet.kstars">Halley's Comet</a></td>
<td bgcolor="#DDDDDD">Track Halley's Comet as it passes through 
perihelion in 1986.
<a href="Halleys_Comet.txt">(more...)</a></td>
<td>Jason Harris</td>
</tr>
</table>

<p>
(Note: To install a script, just save it to your hard drive, and make 
it executable by typing:<br />
<font color="#0000AA"><tt>chmod +x script_name.kstars</tt></font> 
in the script's directory)</p>

<hr />
<p>Jason Harris<br />
<a href="mailto:kstars AT 30doradus DOT org">kstars At 30doradus DOT org</a>
</p>

<?php
  include "footer.inc";
?>

