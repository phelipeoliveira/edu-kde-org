<?php
  $site_root = "../";
  $page_title = 'Compiling KStars from SVN';
  

  include ( "header.inc" );
?>

<h2>Introduction</h2>
<p>
This page provides instructions for downloading, compiling and 
installing the latest development version of the KStars codebase.
<p>
Before you begin, you should think hard about the following question: <br>
<b>Do I really want to compile the latest development code?</b><br>
Before you proceed, keep the following in mind:
<ul>

<li> KStars code is undergoing major changes. Many things now work,
but some things do not.  You should not expect to have a fully
functioning KStars program from the current SVN codebase.  <p>

<li> Getting your system set up to compile the latest KStars code 
involves many steps and a lot of compiling.  You will likely have 
problems along the way.
<p>
</ul>
<p>
KStars development takes place on the <b>KDE Subversion server</b>.  
<a href="http://subversion.tigris.org/">Subversion</a> is a version 
control system, similar to CVS.  It's essentially a code repository 
that keeps track of every incremental version of all files.  You can 
browse the codebase through our <a href="http://websvn.kde.org">web 
interface</a>, but that's not useful for downloading the code, just 
for browsing specific files and checking the commit logs.
<p>

<h2>Dependencies</h2>
<p>
The Qt and KDE libraries are the biggest dependencies of
KStars. Building them can be a difficult task and a steep learning
curve, but it's really almost always a one-time effort. To build
KStars from SVN, you should first have the dependencies in place. <a
href="http://techbase.kde.org/Getting_Started/Build/KDE4">This
article</a> on the KDE techbase is the canonical guide to building the
dependencies. However, you might want to use <a
href="http://techbase.kde.org/Getting_Started/Using_Project_Neon_to_contribute_to_KDE">Project
Neon</a>, which will save a lot of time and effort, but is not
generally recommended, because you cannot fix bugs in the KDE
libraries this way.

The minimum set of dependencies for KStars from KDE are kdelibs (which
in turn, depends on kdesupport and a lot of other things), and
kdebase/runtime (apps and workspace are not necessary in general).

Other dependencies (eigen2, OpenGL) can be used in packaged
form. There are also a lot of optional dependencies that add useful
functionality -- libindi for driving telescopes and other
instrumentation, libcfitsio for FITS file support.

</p>

<h2>Building KStars</h2>

<p>
Once you have the KDE dependencies built, you can get on to building
KStars. If you like to build the whole of KDE-Edu from trunk, you
could do so. The following instructions are minamilistic, and tell you
how to build only KStars:

<pre>
$ cs # cs is not a typo.
$ cd KDE/
$ svn up --depth files kdeedu
$ cd kdeedu
$ svn up cmake data doc libkdeedu kstars # Some of these are superfluous as well
# Install eigen2 and opengl, using your distribution specific tools
$ cmakekde
</pre>

You should now be able to run KStars by just typing "kstars" in the appropriate directory / user account.
</p>

<p>
(If you want to be more minimalistic, you need not have doc, libkdeedu and data to build KStars as of this update. But CMake will complain because those are listed as required directories. You will need to uncomment entries that add those directories.)
</p>

<br />
<br />
(<b>PS:</b> This is not tested. Please report any errors to akarsh DOT simha AT kdemail DOT net)


<hr />
<p>Jason Harris, Akarsh Simha<br />
<a href="mailto:kstars AT 30doradus DOT org"><i>kstars AT 30doradus DOT org</i></a><br />
<a href="mailto:akarsh DOT simha AT kdemail DOT net"><i>akarsh DOT simha AT kdemail DOT net</i></a><br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
