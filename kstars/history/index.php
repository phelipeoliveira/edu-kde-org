<?php
  $site_root = "../../";
  $page_title = 'Great Moments in KStars History  :)';
  
  include ( "header.inc" );
?>

<h2>The Evolution of KStars</h2>

<p>
Feeling nostalgic after the 0.8 release, I recompiled a couple of archived
versions of KStars to recall the good old days, and see how far we've come.
</p>
<table bgcolor="6666BB" cellpadding="12" width="100%">
<tr>
<td>
<img alt="KStars 0.1" src="main-0.1.png"/>
</td><td valign="top">
KStars 0.1: the initial release (05 Apr 2001).  No planets, sun, or moon.
No horizon.  No customizable options of any kind. No right-click menu.
Just 9000 stars, and the Messier objects shown as yellow squares.
</td>
</tr>
<tr>
<td>
<img alt="KStars 0.3" src="main-0.3.png"/>
</td><td valign="top">
KStars 0.3: the foundation is in place (29 Apr 2001).  The only obvious visual
improvement is the addition of constellation lines and labels, but this version
shipped with most of the important stuff in place: right-click popup menu
(including internet image downloads), click-and-drag the map, and customizable
view options.  Still no planets, though.
</td>
</tr>
<tr>
<td>
<img alt="KStars 0.5" src="main-0.5.png"/>
</td><td valign="top">
KStars 0.5: the planets arrive (19 Aug 2001).  Heiko and Thomas have joined
up by now, so things are really starting to come together.  We've got
planets, the horizon, NGC/IC objects, and 40,000 stars.
</td>
</tr>
<tr>
<td>
<img alt="KStars 0.8" src="main-0.8.png"/>
</td><td valign="top">
KStars 0.8: That brings us to today (12 Dec 2001).  With the Milky Way,
coordinate grid lines, a toolbar, and realistic star colors, KStars is well
on its way to becoming a full-featured planetarium.  Lots more to
come...stay tuned!
</td>
</tr>
</table>

<?php
  include "footer.inc";
?>
