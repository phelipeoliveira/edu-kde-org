 <?php
  $appinfo = new AppInfo( "Artikulate" );
  $appinfo->setIcon( "../images/icons/artikulate_32.png", "32", "32");
  $appinfo->setVersion( "0.3" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2013-2014","The Artikulate Developers" );
?>
