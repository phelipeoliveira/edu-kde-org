<?php
  $site_root = "../";
  $page_title = 'KLatin - Screenshots';
  

  include ( "header.inc" );

  $gallery = new EduGallery("KLatin - Screenshots");
  $gallery->addImage("pics/mainwindow_sm.png", "pics/mainwindow.png", 150, 100,  "[Screenshot]", "", "Main window");
  $gallery->addImage("pics/config_sm.png", "pics/config.png", 222, 100,  "[Screenshot]", "", "Configuration dialogue");
  $gallery->startNewRow();
  $gallery->addImage("pics/vocab_sm.png", "pics/vocab.png", 155, 100,  "[Screenshot]", "", "Vocabulary section");
  $gallery->addImage("pics/grammar_sm.png", "pics/grammar.png", 127, 100,  "[Screenshot]", "", "Grammar section");
  $gallery->startNewRow();
  $gallery->addImage("pics/grammar2_sm.png", "pics/grammar2.png", 127, 100,  "[Screenshot]", "", "Grammar section");
  $gallery->addImage("pics/verbs_sm.png", "pics/verbs.png", 137, 100,  "[Screenshot]", "", "Verbs section");
  $gallery->startNewRow();
  $gallery->addImage("pics/results_sm.png", "pics/results.png", 137, 100,  "[Screenshot]", "", "Results dialogue");
  $gallery->addImage("pics/revision_sm.png", "pics/revision.png", 142, 100,  "[Screenshot]", "", "Revision notes");
	
  $gallery->show();
  ?>

 <br />
 <hr width="30%" align="center" />
 <p>
 Author: George Wright<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>

 <?php
   include "footer.inc";
 ?>












