<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$translation_file = "edu-kde-org";
include_once( "functions.inc" );
include_once("../site_includes/applicationpage.inc");

// the following two methods are included by header.inc to generate page names
// they have to be defined first
function function_to_get_sidebar_content()
{
    $app = new EduAppData($_GET['application']);
    $sidebar_content = printSidebar($app, htmlspecialchars($_GET['category']), isset($_GET['development']));
    return $sidebar_content;
}
function function_to_get_page_title()
{
    $app = new EduAppData($_GET['application']);
    $page_title = $app->name()." - ".i18n_var($app->genericName());
    if (isset($_GET['development']) && $_GET['development']) {
        $page_title=$app->name()." - ".i18n_var( "Development Information" );
    }
    return $page_title;
}
include 'header.inc';

$get_category = $_REQUEST[ "category" ];

$app = new EduAppData($_REQUEST['application']);

$development = isset($_REQUEST['development']);

$page_title = function_to_get_page_title($app);
$page_title_extra_html = '<div class="app-icon"><img src="/images/icons/'.nameToUrl($app->name()).'_48.png" alt="'.$app->name().' Icon" /></div>';

if (!$development) {
    printPage($app);
} else {
    printDevelopmentPage($app);
}

include "footer.inc";
?>
