<?php
  $site_root = "../";
  $page_title = 'KHangMan';
  
  include ( "header.inc" );
?>

<?php
  include("khangman.inc");
  $appinfo->showIconAndCopyright();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">Latest news</a> 
]
</div>

<center>

<a href="pics/khangman.png"><img border="0" width="" height="" src="pics/khangman_330.png"></a>

</center>

<h3><a name="description">Description</a></h3>

<p>KHangMan is the classical hangman game. The child should guess a word letter by letter. At each miss, the picture of a hangman appears. After 10 tries, if the word is not guessed, the game is over and the answer is displayed.</p>
<p><img src="pics/games-hint.png" width="22" height="22" alt="Hint" style="float: left" />  A hint can be shown to help you guess the word.
</p>
<p>
<img src="pics/get-hot-new-stuff.png" width="22" height="22" alt="New Stuff" style="float: left right:3px" />  You can use the game to improve your vocabulary in other languages. The words are nouns and available in <b>several languages</b> at the moment including English US, Bulgarian, Catalan, Czech, Danish, German, Greek, British English, Spanish, Estonian, Finnish, French, Irish (Gaelic), Hungarian, Italian, Norwegian (Bokm&#229;l), Dutch, Norwegian (Nynorsk), Occitan, Punjabi, Polish, Portuguese, Brazilian Portuguese, Russian, Slovenian, Serbian, Slovak, Swedish, Tajik, Turkish and Ukrainian. The program will detect which languages are present and enable them. You will also be able to easily download other languages via the Get New Stuff dialog (3 clicks and your data will be installed).</p>
<p>
There are at least <b>4 categories</b> per language: easy, medium, hard and animals which contains only animals nouns. But you can add your own file in the $KDEDIR/share/apps/kvtml/language_code folder or locally in $KDEHOME/share/apps/kvtml/language_code folder and the program will detect it and add it in the categories drop down box.<br />
KHangMan uses the same file format than <a href="kanagram/index.php">Kanagram</a> and shares its files with this program. You can therefore have a lot of files in the Categories menu of KHangMan!</p>

<p><img src="../images/web/tutorial.png" width="40" height="58" alt="Tutorial" style="float: left right:3px " />   <b>Make a list of new words (KHangMan 3.5)</b><br />  <a href="tutorials/khangman-addwords.pdf">pdf tutorial</a><br />  <a href="tutorials/khangman-addwords.odp">odf tutorial</a><br />
</p>

<p>
If you would like to contribute to data in your own language for KDE 4, you can now do so very easily. Please see the Add new language page <a href="add_language/add_language_kde4.php">here</a> for more details.<br />
For KDE 3 data contribution, please look at <a href="../old/khangman/add_language.php">this page</a>.
</p>
<p>
Credits for the data in different languages are on the <a href="languages.php">Languages</a> page.
</p>

<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<br />

<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
