<ul>
  <li>add testcases for class task</li>
  <li>improve speed of prime numbers; Ok, it is not slow at all, but prime number
searching is an interesting topic...</li>
  <li>update doxygen comments, some comments are broken when generating the
docs</li>
  <li>translate more code comments and variable names into English</li>
  <li>check why loading takes that long</li>
</ul>
