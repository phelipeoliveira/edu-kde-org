<?php
  $site_root = "../";
  $page_title = 'KBruch';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KBruch" );
  $appinfo->setIcon( "../images/icons/kbruch_32.png", "32", "32" );
  $appinfo->setVersion( "4.0" );
  $appinfo->setCopyright( "2002", "Sebastian Stein" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Sebastian Stein", "seb.kde AT hpfsc DOT de" );
  $appinfo->addContributor("Sebastian Stein", "seb.kde AT hpfsc DOT de", "Current maintainer" );
  $appinfo->show();
?>

<br />
<h3>Description</h3>

<p>KBruch is a small program to practice calculating with fractions. Therefore
4 different exercises are offered.</p>
<ol>
	<li><img src="pics/ox32-action-kbruch_exercise_common.png" width="32" height="32" alt="Common" style="float:left right:3px" /> Exercise Fraction Task - in this exercise you have to solve a given
fraction task.  You have to enter numerator and denominator. This is the main
exercise. The difficulty of this task can be influenced by the user. The user
can decide if he wants to solve tasks with addition/substraction and/or
multiplication/division. Also he can set the number of fractions and the maximum
size of the main denominator.</li>
	<li><img src="pics/ox32-action-kbruch_exercise_compare.png" width="32" height="32" alt="Compare" style="float:left right:3px" /> Exercise Comparison - in this exercise you have to compare the size of 2
given fractions.</li>
	<li><img src="pics/ox32-action-kbruch_exercise_conversion.png" width="32" height="32" alt="Conversion" style="float:left right:3px" /> Exercise Conversion - in this exercise you have to convert a given number
into a fraction.</li>
	<li><img src="pics/ox32-action-kbruch_exercise_factorisation.png" width="32" height="32" alt="Factorization" style="float:left right:3px" /> Exercise Factorization - in this exercise you have to factorize a given
number into its prime factors. Factorization is important while finding the main
denominator of 2 fractions.</li>
</ol>
<br />
<p>
KBruch provides a handbook describing the different exercises and the general
usage.<br />
KBruch is shipped with KDE since the major version 3.2 (released February 2004).
To get the latest development version see the Obtain section.
</p>

<h3>TODO List</h3>

<?php
	include("./todo.inc")
?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>







