<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
 
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
 
<xsl:template match="/knewstuff">
    <table border="0">
    <tr>
        <th>Gender</th>
        <th>Description</th>
    </tr>
        <xsl:apply-templates select="stuff">
            <xsl:sort select="name" />
        </xsl:apply-templates>
    </table>
</xsl:template>
 
<xsl:template match="stuff">
    <tr>
        <td><img src="{preview}" border="0" style="vertical-align:middle;" /></td>
        <td style="vertical-align:middle;"><p>
        <b><xsl:value-of select="name"/></b><br />
        <i><xsl:value-of select="summary"/></i><br />
        License: <xsl:value-of select="licence"/></p>
        <audio src="{payload[@lang='ogg']}" controls="controls">Your browser does not support the audio element.</audio><br />
        <p>Download <a href="{payload[@lang='zip']}" >for Marble</a> or <a href="{payload[@lang='tomtom']}" >for TomTom</a>.</p></td>
    </tr>
</xsl:template>
 
</xsl:stylesheet>
