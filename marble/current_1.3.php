

<?php
  $site_root = "../";
  $page_title = 'Marble 1.3: Visual Changelog';

  include ( "header.inc" );

  $submenu->show();
?>
<p>
Marble 1.3 <a href="http://edu.kde.org/marble/download.php">was released</a> on January 25th, 2012 as part of the KDE 4.8 release. See the <a href="http://edu.kde.org/marble/download.php">Download section</a> for Marble
packages.
In the good tradition of recent releases, we have collected those changes
directly visible to the user. Please enjoy looking over the new and noteworthy:
</p>

<h4>Elevation Profiles</h4>

<p>Estimating the incline is important to hikers and bikers when planning a route. Marble's new elevation profile shows the height profile of any route worldwide in an interactive info box. After activating the <i>Elevation Profile</i> info box in Marble's menu, height profiles are automatically displayed and updated as you change the route. This feature was developed by Niko Sams and Florian E&szlig;er. It is available in all Marble versions (Desktop and Mobile).</p>

<dl>
  <dt><iframe width="640" height="360" src="http://www.youtube-nocookie.com/embed/sIZB35MWZrI?rel=0&amp;hd=1" frameborder="0" allowfullscreen></iframe></dt>
  <dd><i>Do you like steep routes? Or rather avoid them? Marble's new elevation profile will help you in either case.</i></dd>
</dl>

<h4>Satellites - Earth Seen from Space</h4>

<p>
Did you ever wonder how position tracking works in Marble? The answer, as you may already know, is GPS, the Global Positioning System. One of its core parts are satellites traveling around the earth. You might be interested where those are, since their position highly influences the accuracy of your estimated current position. Guillaume Martres helps answering that question: Activate the <i>Satellites</i> Online Service and Marble shows you many of the artificial satellites. You can configure which satellite types are visible (like GPS or weather related ones), show their orbit and even select any of them to attend in a virtual flight!
</p>

<p>
This feature was developed during an ESA SOCIS project, the first <i>Summer of Code in Space</i> offered this year by the European Space Agency (ESA). We'd like to thank both Guillaume for his excellent work and the ESA for offering us the chance to be one of the mentoring organizations. Satellites can be viewed in Marble's Desktop versions and in Marble Touch on the Nokia N9/N950.
</p>

<dl>
  <dt><iframe width="640" height="360" src="http://www.youtube-nocookie.com/embed/YlKMPEAf-mk?rel=0&amp;hd=1" frameborder="0" allowfullscreen></iframe></dt>
  <dd><i>Marble can now display satellites from many different categories, show their orbits and even track them.</i></dd>
</dl>

<h4>Plasma Runner Integration</h4>

<p>KRunner, the "Alt+F2" tool of Plasma, helps searching and launching files and applications. Thanks to Friedrich Kossebau it is now aware of Marble and can open its bookmarks and coordinates. So next time you stumble upon some coordinates on a website or get an invitation to a party by email, just paste the coordinates in krunner and view the place in Marble.</p>

<dl>
  <dt><iframe width="640" height="360" src="http://www.youtube-nocookie.com/embed/auxbMI4N6is?rel=0&amp;hd=1" frameborder="0" allowfullscreen></iframe></dt>
  <dd><i>Open your Marble bookmarks and any coordinates from krunner.</i></dd>
</dl>

<h4>Extended OpenStreetMap Vector Rendering</h4>

<p>
Similar to how the Wikipedia community gathered an incredible amount of knowledge outperforming commercial encyclopedias, the OpenStreetMap contributors are busy mapping each and every part of the planet to create the data for the many OpenStreetMap based maps. Marble integrated them early (e.g. Mapnik, Open Cycle Map, Hike &amp; Bike Map, Osmarender) in the popular server based tiling schemes where the map consists of many small images placed next to each other similar to a puzzle. This approach has several advantages, but customizations of the final map are hardly doable. Moreover the images for larger regions require a huge amount of disk space which makes offline usage difficult. While this is often not an issue for desktop computers, things change for mobile devices. And since we plan to bring Marble to the devices you may be using today or tomorrow, we are working on OpenStreetMap vector rendering support that needs little disk space, little computational effort and can be customized to the context you
use it in (like motorcar navigation or hiking).
</p>

<p>
Towards this goal Konstantin Oblaukhov made great progress during his Google Summer of Code project. Marble can now open .osm files and show a huge amount of the elements they contain.
<!--
We have assembled a handful of sample files for you to give it a try. TODO: Add links here.
-->
</p>

<dl>
  <dt><iframe width="640" height="360" src="http://www.youtube-nocookie.com/embed/QnZDkxj2SUU?rel=0&amp;hd=1" frameborder="0" allowfullscreen></iframe></dt>
  <dd><i>Many OpenStreetMap elements can now be imported and rendered by Marble on street level.</i></dd>
</dl>

<p>
Of course we plan to continue working on the OpenStreetMap vector rendering support such that future Marble versions improve the rendering on different zoom levels, can be customized and download the data automatically from KDE servers. Stay tuned.
</p>

<h4>Marble Touch - Marble on the Nokia N9/N950</h4>

<p>Marble's first end-user ready mobile version made its debut on the Nokia N900 in 2010. Aiming to create a more fluid and fun to use predecessor, we have been working on <i>Marble Touch</i>, a mobile version of Marble that is based on new Qt technologies (Qt Quick and Qt Components) and runs on the Nokia N9/N950. A prominent change is the introduction of <i>activities</i> which optimize the user interface towards common tasks like searching or routing.</p>

<dl> <dt> <a href="http://nienhueser.de/marble/marble-touch-routing.jpg"><img border="0"
width="400" height="625" src="./screenshots/1.3/marble-touch-routing.jpg"
alt="Marble Touch on the Nokia N950"></a> </dt> <dd><i>Marble Touch on the Nokia N950 in the Routing activity.</i></dd>
</dl>

<p>Please note that the initial version 1.3.0 of Marble Touch does not include all features the N900 version of Marble offers. We do plan however to add these and other features gradually in monthly feature releases.</p>
<p><b>Update:</b> Marble Touch is now available in the <a href="http://store.ovi.com/content/249807">Nokia Store</a>.

<dl>
  <dt><iframe width="640" height="360" src="http://www.youtube-nocookie.com/embed/iCrcWyYaiwo?hd=1" frameborder="0" allowfullscreen></iframe></dt>
  <dd><i>Marble Touch runs on the Nokia N9/N950. Versions for Plasma Active and Android are planned for the future.</i></dd>
</dl>

<p>
Even though Marble Touch is deemed to be the predecessor of the Maemo version, we still maintain the wide-spread N900 version, which is released in version 1.3 in parallel. Future plans for Marble Touch include optimizations for tablet devices. Thanks to the usage of Qt Components and thanks to the Plasma Active team working on compatible Plasma Active Components, Marble Touch on Plasma Active will follow soon. A version for Android is planned as well.
</p>

<h4>And More ...</h4>
<ul>
<li>
New <a href="http://edu.kde.org/marble/maps.php">Maps for Download</a> are available: OpenStreetMap themes, historic maps and maps for other planets, most of them created by students during Google Code-In. You can install them immediately using the <i>Get New Maps</i> feature available from Marble's <i>File</i> menu or the Preferences page in Marble Touch.
</li>
<li>
The number of <i>voice navigation speakers</i> has risen considerably, so chances are high you're able to listen to Marble giving you navigation instructions in your native language. Check out the <a href="http://edu.kde.org/marble/speakers.php">Voice Guidance Speakers</a> website to see (and hear) them all.
</li>
<li>Your virtual globe now behaves even more like a desktop globe thanks to <i>kinetic spinning</i> implemented by Ariya Hidayat. Watch e.g. the Satellites screencast above to see it in action (compare the behavior of the mouse to the movement of the globe).</li>
<li>Two new GPS related info boxes have been added. Just like other info boxes, they can be enabled in the <i>View / Info Boxes</i> menu.</li>
<li>Support for file formats has been modularized; parsing is now done in threaded runners. Developers can <a href="http://techbase.kde.org/Projects/Marble/Routing/BasicRouting">perform routing tasks</a> from the library.</li>
<li>As usual, many bugfixes, performance improvements, enhancements and refactorings under the hood have been done in order to keep our code in best shape ;-) 26 developers made 1031 commits, adding 66,291 and removing 55,948 lines of code. Marble in KDE 4.8 now measures 99,489 source lines of code in total, 13,544 more than in KDE 4.7 (generated using David A. Wheeler's 'SLOCCount').</li>
</ul>

<p>
There's a factsheet available that gives an overview for the latest full feature set:

<table cellspacing="10">
<tr valign="top">
<td>
    <div style="width:300px" id="__ss_10742037"> <strong style="display:block;margin:12px 0 4px"><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-english" title="Marble Virtual Globe 1.3 Factsheet (English)" target="_blank">Marble Virtual Globe 1.3 Factsheet (English)</a></strong> <iframe src="http://www.slideshare.net/slideshow/embed_code/10742037" width="300" height="315" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe></div>
</td>
<td>
    <ul>
	<li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-english">English</a>
	<li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-spanish">Spanish</a>
	<li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-german">German</a>
	<li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-french">French</a>
	<li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-marble-virtual-globe-13-factsheet-hindi">Hindi</a>
    </ul>
    <br>
    <ul>
	<li><a href="http://www.slideshare.net/marbleglobe/marble-for-developers-factsheet">For Developers</a>
    </ul>
</td>
</tr>
</table>

<p>

Join our Marble Page in your favorite social network:

<table cellspacing="10">
<tr valign="bottom"><td>
<a href="http://opendesktop.org/groups/?id=439"><img src="http://static.opendesktop.org/img/headers/header1_10_1.jpg" class="showonplanet" /></a>
</td><td>
<p><!-- Facebook Badge START --><a href="http://www.facebook.com/marbleglobe" target="_TOP" title="Marble (Virtual Globe)"><img src="http://badge.facebook.com/badge/112069922186463.730.115653362.png" width="120" height="177" style="border: 0px;" /></a><br/><!-- Facebook Badge END -->
</td><td>
<a href="https://plus.google.com/109002795119670287274?prsrc=3" style="text-decoration: none; color: #333;"><div style="display: inline-block; *display: inline;"><div style="text-align: center;"><img src="https://ssl.gstatic.com/images/icons/gplus-64.png" width="64" height="64" style="border: 0;"/></div><div style="font: bold 13px/16px arial,sans-serif; text-align: center;">Marble</div><div style="font: 13px/16px arial,sans-serif;"> on Google+ </div></div></a>
</td>
</tr>
</table>

<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
   include "footer.inc";
?>
