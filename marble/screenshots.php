<?php
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop('Marble - Screenshots');

  include ( "header.inc" );
  
  $submenu->show();

?>
<p><?php i18n( "Here we present a few screenshots of the latest stable version of Marble.
These screenshots provide a visual summary of the functionality that Marble offers.
<p>If you just want to see the most recent changes have a look at the
<a href=\"http://edu.kde.org/marble/current.php\">Visual Changelog</a>." ); ?>
<br>
<p>
<dl> <dt> <a href="./screenshots/generic/marble-measure-distance.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-measure-distance_thumb.png" alt="Marble: Measuring Distances on the globe"></a> </dt> <dd><i><?php i18n( "Measuring Distances on the globe" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-measure-distance2.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-measure-distance2_thumb.png" alt="Marble: Measuring Distances on a flat map"></a> </dt> <dd><i><?php i18n( "Measuring Distances on a flat map" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-night-day.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-night-day_thumb.png" alt="Visualizing Night and Day on Satellite View"></a> </dt> <dd><i><?php i18n( "Visualizing Night and Day on Satellite View" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-mars-flat-night-day.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-mars-flat-night-day_thumb.png" alt="... also works for Mars and a flat map"></a> </dt> <dd><i><?php i18n( "... also works for Mars and a flat map" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-moon-tycho.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-moon-tycho_thumb.png" alt="Crater Tycho on the Moon"></a> </dt> <dd><i><?php i18n( "Crater Tycho on the Moon" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-tour-eiffel.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-tour-eiffel_thumb.png" alt="C'est magnifique!"></a> </dt> <dd><i><?php i18n( "C'est magnifique!" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-linkoeping.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-linkoeping_thumb.png" alt="Linköping with OpenStreetMap data"></a> </dt> <dd><i><?php i18n( "Linköping with OpenStreetMap data" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-history.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-history_thumb.png" alt="Think back how people imagined the world in 1689"></a> </dt> <dd><i><?php i18n( "Think back how people imagined the world in 1689" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-kml.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-kml_thumb.png" alt="A KML file of the United States displayed using Marble"></a> </dt> <dd><i><?php i18n( "A KML file of the United States displayed using Marble" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-turkey.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-turkey_thumb.png" alt="A topographics map of Turkey"></a> </dt> <dd><i><?php i18n( "A topographic map of Turkey" ); ?></i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/generic/marble-ghns.png"><img border="0" width="410" height="294" src="./screenshots/generic/marble-ghns_thumb.png" alt="Downloading new maps in Marble"></a> </dt> <dd><i><?php i18n( "Downloading new maps in Marble" ); ?></i></dd> </dl>
</p>



 <br />
 <hr width="30%" align="center" />
 <p>
 <?php i18n( "Last update:" ); ?> <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>

 <?php
   include "footer.inc";
 ?>
