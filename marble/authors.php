<?php
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop('Marble Authors');

  include ( "header.inc" );

  $submenu->show();

  $appinfo = new AppInfo( "Marble" );
  $appinfo->setIcon( "./logo/marble-logo-72dpi.png" );
  $appinfo->setVersion( "0.10.3" );
  $appinfo->setLicense("lgpl");
  $appinfo->setCopyright( "2005", "The Marble Project" );

  $appinfo->addAuthor( "Torsten Rahn", "tackat AT kde DOT org", i18n_var( "Original Author and Maintainer" ) );
  $appinfo->addAuthor( "Inge Wallin", "inge AT lysator DOT liu DOT se", i18n_var( "Co-Maintainer and Architect" ) );
  $appinfo->addContributor("Dennis Nienhüser", "earthwings AT gentoo DOT org", i18n_var( "Routing, Maemo5 support" ) );
  $appinfo->addContributor("Thibaut Gridel", "tgridel AT free DOT fr", i18n_var( "Marble architecture, Plugins, Maemo5 support" ) );
  $appinfo->addContributor("Bernhard Beschow", "bbeschow AT cs.tu-berlin DOT de", i18n_var( "Marble architecture, OpenGL, Maemo5 support" ) );
  $appinfo->addContributor("Jens-Michael Hoffmann", "jensmh AT gmx DOT de", i18n_var( "OpenStreetMap Support" ) );

  $appinfo->addContributor("Utku Aydin", "utkuaydin34 AT gmail DOT com", i18n_var( "Map Wizard, Earthquake Plugin" ) );
  $appinfo->addContributor("Daniel Marth", "danielmarth AT gmx DOT at", i18n_var( "Extended Plugin configuration" ) );

  $appinfo->addContributor("Christophe Leske", "info AT multimedial DOT de", i18n_var( "Windows packages" ));
  $appinfo->addContributor("Sebastian Wiedenroth", "wiedi AT frubar DOT net", i18n_var( "Mac OS X packages" ));

  $appinfo->addContributor("Patrick Spendrin", "ps_ml AT gmx DOT de", i18n_var( "Marble on Windows, KML Support" ));
  $appinfo->addContributor("Bastian Holst", "bastianholst AT gmx DOT de", i18n_var( "Online Service Support, Maemo5 support" ));
  $appinfo->addContributor("Andrew Manson", "g.real.ate AT gmail DOT com", i18n_var( "Proxy Support" ));
  $appinfo->addContributor("Eckhart Wörner", "ewoerner AT kde DOT org", i18n_var( "GPS Plugins, GeoClue Bindings" ));
  $appinfo->addContributor("Henry de Valence", "hdevalence AT gmail DOT com", i18n_var( "Marble Plasmoid, Temperature and Precipitation maps" ));
  $appinfo->addContributor("Simon Edwards", "simon AT simonzone DOT com", i18n_var( "Python Bindings" ));
  $appinfo->addContributor("Magnus Valle", "wiscados AT gmail DOT com", i18n_var( "Historical Maps" ));

  $appinfo->addContributor("Pino Toscano", "toscano DOT pino AT tiscali DOT it>", i18n_var( "IRC developer support & KIO plugin" ));
  $appinfo->addContributor("David Roberts", "dvdr18 AT gmail DOT com", i18n_var( "Sun shading, real-time cloud cover" ));
  $appinfo->addContributor("Nikolas Zimmermann", "wildfox AT kde DOT org", i18n_var( "GeoData XML parsing framework" ));
  $appinfo->addContributor("Wes Hardaker", "marble AT hardakers DOT net", i18n_var( "APRS plugin" ));

  $appinfo->show();
?>
<br>
<p>
<dl> <dt> <a href="./poster/marble_changeworld.jpg"><img border="0" height="263" src="./poster/marble_changeworld_thumb.jpg" alt="Change The World? Together, I like our odds."></a> </dt></dl>
</p>

  <p><?php i18n( "The Marble Team would like to thank its members who participated in the Google Summer of Code for their successful work on Marble:" ); ?>

<H4><?php i18n( "Google Summer of Code 2011" ); ?></H4>
  <p>
    Konstantin Oblaukhov <a href="mailto:oblaukhov.konstantin@gmail.com">oblaukhov dot konstantin at gmail dot com</a><br>
    <i>Project: OpenStreetMap Vector Rendering</i>
  </p>
  <p>
    Daniel Marth <a href="mailto:danielmarth@gmx.at">danielmarth at gmx dot at</a><br>
    <i>Project: Marble Touch on MeeGo</i>
  </p>

<H4><?php i18n( "Google Summer of Code 2010" ); ?></H4>
  <p>
    Gaurav Gupta <a href="mailto:1989.gaurav@gmail.com">1989 dot gaurav at gmail dot com</a><br>
    <i>Project: Bookmarks</i>
  </p>
  <p>
    Harshit Jain <a href="mailto:hjain.itbhu@gmail.com">hjain dot itbhu at gmail dot com</a><br>
    <i>Project: Time Support</i>
  </p>
  <p>
    Siddharth Srivastava <a href="mailto:akssps011@gmail.com">akssps011 at gmail dot com</a><br>
    <i>Project: Turn-by-turn Navigation</i>
  </p>

<H4>Google Summer of Code 2009</H4>
  <p>
    Bastian Holst <a href="mailto:bastianholst@gmx.de">bastianholst AT gmx DOT de</a><br>
    <i>Project: Weather Plugin for Marble</i>
  </p>
  <p>
    Andrew Manson <a href="mailto:g.real.ate@gmail.com">g.real.ate AT gmail DOT com</a><br>
    <i>Project: OpenStreetmap Annotations for Marble</i>
  </p>

<H4>Google Summer of Code 2008</H4>
  <p>
    Patrick Spendrin <a href="mailto:patrick_spendrin@gmx.de">patrick_spendrin AT gmx DOT de</a><br>
    <i>Project: Vector Tiles for Marble</i>
  </p>
  <p>
    Shashank Singh <a href="mailto:shashank.personal@gmail.com">shashank.personal AT gmail DOT com</a><br>
    <i>Project: Panoramio support for Marble</i>
  </p>

<H4>Google Summer of Code 2007</H4>
  <p>
    Carlos Licea <a href="mailto:carlos.licea@kdemail.net">carlos.licea AT kdemail DOT net</a><br>
    <i>Project: Equirectangular Projection ("Flat Map")</i>
  </p>
  <p>
    Andrew Manson <a href="mailto:g.real.ate@gmail.com">g.real.ate AT gmail DOT com</a><br>
    <i>Project: GPS Support for Marble</i>
  </p>
  <p>
    Murad Tagirov <a href="mailto:tmurad@gmail.com">tmurad AT gmail DOT com</a><br>
    <i>Project: KML Support for Marble</i>
  </p>
  <p>... and of course we'd like to thank the people at Google Inc. for
making these projects possible.

<H4><?php i18n( "Developers" ); ?></H4>
  <p>
    <i><?php i18n( "Development &amp; Patches:" ); ?></i> Simon Schmeisser, Claudiu Covaci, Jan Becker, Pino Toscano, Stefan Asserh&auml;ll, Laurent Montel, Prashanth Udupa, Anne-Marie Mahfouf, Josef Spillner, Frerich Raabe, Frederik Gladhorn, Fredrik H&ouml;glund, Albert Astals Cid, Thomas Zander, Joseph Wenninger, Kris Thomsen, Daniel Molkentin
  </p>
  <p>
    <i><?php i18n( "Platforms &amp; Distributions:" ); ?></i> Tim Sutton, Christian Ehrlicher, Ralf Habacker, Steffen Joeris, Marcus Czeslinski, Marcus D. Hanwell, Chitlesh Goorah.
  </p>
  <p>
    <i><?php i18n( "Artwork:" ); ?></i> Nuno Pinheiro, Torsten Rahn
  </p>

<H4><?php i18n( "Credits" ); ?></H4>
  <p>
    <i><?php i18n( "Various Suggestions &amp; Testing:" ); ?></i> Stefan Jordan, Robert Scott, Lubos Petrovic, Benoit Sigoure, Martin Konold, Matthias Welwarsky, Rainer Endres, Luis Silva, Ralf Gesellensetter, Tim Alder
  </p>
  <p>
    <i><?php i18n( "Editing" ); ?></i>: Dawn Hardaker
  <p>
    <?php i18n( "We'd especially like to thank John Layt who provided an important source of inspiration by creating Marble's predecessor \"Kartographer\"." ); ?>
  </p>
<br>
<br />
<hr width="30%" align="center" />
<p><?php i18n( "Last update:" ); ?> <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

