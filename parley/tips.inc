<!--
<span style="font-weight:bold">Frequently Asked Questions</span><br/>
<ul>
	<li>I might post something here.</li>
</ul>
-->

<span style="font-weight:bold">Editing Vocabulary</span><br/>
<ul>
	<li>Select multiple rows by pressing a mousebutton on the first entry and dragging. Alternatively you can press shift or ctrl when selecting entries.</li>
	<li>When you have selected multiple rows, you can for example change the word type for the entire selection. Simply use the edit dialog. Even when that dialog is open, you can change the selection.</li>
	<li>Resetting the grades: Use "Edit Entry" (right click on the word) choose the language combination you want and click reset grades. You can do this for any selection. To reset all grades simply use "Select All" and reset the grades.</li>
	<li>Search for words beginning with "the" by typing "^the" into the search field. Words ending with "end" can be selected by entering "end$".</li>
	<li>Search for a certain word type e.g. "noun" by entering "type:noun" in the search line.</li>
	<li>It is possible to import version <b>1</b> files from www vokabeln de. If they have missing or strange letters, converting them to unicode might help. On the command line use "$recode ISO_8859-1..UTF8 filename.voc". Open the file with Parley and save again in the native kvtml format to enable all features properly.</li>
	<li>Images and soundfiles are saved relative to the document if they are in the same folder or in a subfolder. So to copy a document including images, put the images in the same folder or make a subfolder containing the images. Currently only local files work for images.</li>
	<li>Soundfile locations are saved the same way as images but using remote files like "http://something.org/soundfile.ogg" works just fine.</li>
	<li>Sound and <b>ogg</b>: It seems that xine-lib has problems with short ogg files (<2.7 seconds). So if ogg files don't work (KDEs Phonon currently uses xine as backend for sound) test them in the xine player. If they don't work there, please bug the xine people.</li>
</ul>


<span style="font-weight:bold">Practicing</span><br/>
<ul>
	<li>To select which words you practice check the lessons on the left.</li>
	<li>Use "Practice"->"Configure Practice" to set the languages and type of test. For example Multiple Choice or Written (you type the word) or Grammar, etc.</li>
	<li>If you have to switch the type of practice often, you can add "Configure Practice" as button to the toolbar. You can also assign it a shortcut.</li>
	<li>You probably want to enable blocking in the "Settings"->"Configure Parley" menu.</li>
	<li><b>Blocking:</b> if you had an entry right, it will not be asked for the time set here.</li>
	<li><b>Conjugations:</b> use "Language"->"Articles and pronouns". To set up the pronouns first. They will be displayed in the edit and practice dialogs. Here you can also enable dual conjugations and select for each language if conjugations differentiate male/female/neuter.</li>
</ul>
