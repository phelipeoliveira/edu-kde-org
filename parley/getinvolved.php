<?php 
  $site_root = "../";
  $page_title = "Get Involved with Parley";

  include ( "header.inc" );
?>

<img src="pics/ox256-app-parley.png" align="right" vspace="10" hspace="20" >
<p>Welcome to Parley. By joining this project, you will be part of a small team working to create a fun and easy to use, yet feature rich vocabulary (and spaced repetition in general) trainer. There are many different ways you can help to improve Parley.</p>
<br/>
 <p>
  The KDE Education team is very open to new people.
  Join us and have a chat with us on IRC (server: irc.freenode.net, channel: #kde-edu)
  or contact us via the <a href="https://mail.kde.org/mailman/listinfo/kde-edu">kde-edu mailing-list</a>.
 </p>

 <p><a href="https://mail.kde.org/mailman/listinfo/parley-devel">Join the Parley development mailing list.</a>
 </p>

<h3><a name="kvtml">Vocabulary files</a></h3>
 <p>
  Please help by contributing vocabulary files!
  See see the <a href="../contrib/kvtml2/kvtml2.php">vocabulary files page</a> for more information.
 </p>

<h2>Bugs and Features</h2>
 <p>
  One thing that helps tremendously to improve Parley is your feedback!
  Already many great improvements were suggested by users.
  Some ideas are so great, that it's a pleasure to implement them.
  To help getting features and bugs organized, please use the
  <a href="https://bugs.kde.org/wizard.cgi">bug and feature reporting wizard</a> at
  <a href="https://bugs.kde.org">bugs.kde.org</a>.
 </p>

<h3><a name="documentation">Documentation</a></h3>
 <p>
  You can also help by updating the online help.
  By providing useful and up-to-date documentation,
  you will make a big impact on helping people understand parley.
 </p>

<h3><a name="art">Artwork and Design</a></h3>
 <p>
  Parley needs artists! There are many fun things to improve.
  The practice dialogs for example need to be better designed.
  Maybe different themes will be possible at some time?
  I'm sure new icons will be necessary as well.
  And since the vocabulary files can contain images, new possibilities are yet to be discovered.
 </p>

<h3><a name="sound">Sound</a></h3>
 <p>
  As the sound support improves, it will become interesting to add sound files to vocabulary documents.
  Maybe you could record some pronunciation files in your language?
  Here more research is still necessary. How should sound integration be improved in the future.
  Ideas are very welcome.
 </p>

<h3><a name="webpage">Homepage</a></h3>
 <p>
  Improving and maintaining this web site would be a nice thing to do.
 </p>

<h3><a name="xslt">HTML, CSS and/or XML/XSL/XSLT/XPath</a></h3>
 <p>
  The file format used by Parley is a XML format.
  If you have some HTML and maybe even XSL knowledge you could really help us.
  To print files so far we rely on a very simple and poor implementation.
  It would really be great if you invested some time to improve the simple xsl file I created a while ago.
  With this file the vocabulary contained in a kvtml document can be displayed as HTML table.
  It would be great to have different style options available and for example create flash cards ready for printing.
  This is quite rewarding because the result is immediately visible and it's fun thinking about good layouts for the HTML.
  The current simple
  <a href="http://websvn.kde.org/trunk/KDE/kdeedu/libkdeedu/keduvocdocument/docs/kvtml_html_stylesheet.xsl?view=markup">stylesheet for kvmtl documents</a>
  and an
  <a href="http://websvn.kde.org/trunk/KDE/kdeedu/libkdeedu/keduvocdocument/docs/basic_vocabulary_en2de.kvtml?view=markup">example vocabulary document</a>.
 </p>

<h3><a name="development">Development</a></h3>
 <p>
  You like to write programs and feel like spending some time improving Parley?
  Let me know, I'm looking forward to help you getting started!
  Become involved in a fresh moving open source project.
  There are big changes yet to come!
  You can
  <a href="http://websvn.kde.org/trunk/KDE/kdeedu/parley/">browse the source code online</a>.
  Refer to the <a href="./obtain.php">obtain page</a> how to get Parley and the source code.
  There is a <a href="http://websvn.kde.org/trunk/KDE/kdeedu/parley/TODO?view=markup">TODO file</a>.
 </p>

<h2>Research</h2>
  <p>
  How about contacting schools, web pages and others,
   to know what they expect and need and what we can get from them?
  One great thing would be to have a wiktionary.org integration in Parley.
  And how would automatically fetching translations for words be?
  Find out about who would be willing to collaborate to get more vocabulary files made!
  Another aspect would be to look at language related topics.
  There are still many things to improve, so join us!
  </p>

<br/>
 <p>
  Join us and have a chat with us on IRC (server: irc.freenode.net, channel: #kde-edu)
  or contact us via the <a href="https://mail.kde.org/mailman/listinfo/kde-edu">kde-edu mailing-list</a>.
  This is a great opportunity to get involved with an open source project!
 </p>

<?php require 'footer.inc'; ?>
