<ul>
<li><span style="font-weight:bold">Write the documentation</span></li>
<li><span style="font-weight:bold">Work about the management of the verbs that should be learned</span><br/>
<ul>
<li>What happens to the verbs for which the user entered wrong solutions?</li>
<li>How long does it take until they are presented again to the user?</li>
<li>Will the way I did it in the old kverbos still be interesting?</li>
</ul>
</li>
<li><span style="font-weight:bold">Add more 'What's this' information to the different GUI items.</span></li>
<li><span style="font-weight:bold">Make that the verb input line in the conjugation lookup trainer deletes itself when a new verb is going to be entered.</span></li>
<li><span style="font-weight:bold">Sometimes when the puzzle is solved some more points remain to be added to the feedback. Make that the new puzzle can be selected and that the pending points are added to the new pussle.</span><br/>
<ul>
<li>The feedback could have a member m_pending, that holds the points</li>
<li>Maybe the feedback could have a function to report more than one point at a time</li>
</ul>
</li>
<li><span style="font-weight:bold">In the verbs selection dialog there should be a third tab which has a table where the user can get an
easy overwiew which verbs he has selected. Maybe on this tab there could be the possibility to deselect verbs.</span></li>
<li><span style="font-weight:bold">The menu entry settings</span><br/>
<ul>
<li>Should it be disabled during login and learning?</li>
<li>If it isn't disabled the clock should be stopped when it is called during learning</li>
<li>Should there be a messagebox that the changes will be used the next time the program starts?</li>
</ul>
</li>
<li><span style="font-weight:bold">In the verb conjugation trainer the red font color isn't set 
correctly all the time. Especially in the spanish infinitiv input line.</span></li>

</ul>