<?php
  $site_root = "../";
  $page_title = 'Create a Standalone-Package';
  
  include ( "header.inc" );
?>

<h4>Calling the script</h4>
<p>You start the script with ./release_app.rb. There are five
commandline-options:
</p>
<ul>
	<li>--noi18n or -i, do not pack translations and documentation</li>
	<li>--nogpg or -g, do not create a signature with GnuPG</li>
	<li>--libkdeedu or -l, pack libkdeedu (needed for some applications)</li>
	<li>--widgets or -w, pack the kdeedu-widgets (like the clock)</li>
	<li>--help or -h, issue a help</li>
</ul>

<h4>Which app do you want to pack?</h4>
<p>To make this script as easy as possible this script will show you a list
of apps. Simply choose the one you want to pack from the list and click on "Ok".</p>
<p><img src="pics/release_app_1.png" alt="" /></p>

<h4>Which version should the tarball have?</h4>
<p>The script will result in a tar.bz2-file. In this example, it will be named
<i>kalzium-3.5-preview1.tar.bar</i>.</p>
<p><img src="pics/release_app_2.png" alt="" /></p>

<h4>The work starts...</h4>
<p>Now you can sit back and watch the script doing its work. It will inform
you about what it is currently doing.</p>
<p>
-  -  - Start packing kalzium -  -  -<br />
- - - - Checking out libkdeedu - - - -<br />
-  -  - Checking out kalzium -  -  - -<br />
- - Checking out kde-commons/admin - -<br />
<br />
-  -  -  -  -  - l10n -  -  -  -  -  -<br />

</p>
<h4>The documentation is fetched...</h4>
<p>Only available docs are fetched. You see a "foo done" for ever 
working language. All languages which are not listed here failed. In
99.99% this means that there are no translations for that language</p>
da done<br />
de done<br />
es done<br />
et done<br />
fr done<br />
hu done<br />
it done<br />
nl done<br />
pt done<br />
pt_BR done<br />
ru done<br />
sv done<br />
...<br />
<h4>The translations are packed...</h4>
<p>For every language which has a translation of the app you are
packing you will get a status-message. This will look like this:</p>
Copying af's kalzium.po over ..  done<br />
Copying ar's kalzium.po over ..  done<br />
Copying az's kalzium.po over ..  done<br />
Copying bg's kalzium.po over ..  done<br />
Copying bn's kalzium.po over ..  done<br />
Copying br's kalzium.po over ..  done<br />
Copying bs's kalzium.po over ..  done<br />
...<br />
<h4>GnuPG-Signing</h4>
<p>It is always good to sign you package. If you didn't start the script 
with <i>--nogpg</i> you will be asked for you GnuPG-password. This will create
a .sig-file.</p>
<p>
- - - Signing with your GnuPG-key - - -<br />
</p>
<h4>Finish...</h4>
<p>At the end of the script will will get some information you might
need if you announce you new package. This looks similar to this:</p>
<p>
=====================================================<br />
Congratulations :) kalzium 3.5-preview1 tarball generated.<br />
<br />
MD5 checksum: 3655d813c146cbdf9a1c79e0f6f52994  kalzium-3-5-preview1.tar.bz2<br />
The user can verify this package with<br />
<br />
gpg --verify kalzium-3.5-preview1.tar.bz2.sig kalzium-3.5-preview1.tar.bz2<br />

</p>

<br/>
<br/>
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
