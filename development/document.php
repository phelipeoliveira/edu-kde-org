<?php
  $site_root = "../";
  $page_title = "Documentation Tutorial";
  
  include ( "header.inc" );
?>

<div id="quicklinks">[
	<a href="#basics">Basic Rules</a> |
	<a href="#folder_content">doc Folder Contents</a> |
	<a href="#translation">Translation</a> |
	<a href="#mailinglists">Mailing Lists</a> |
	<a href="#things-to-remember">Things to remember</a> |
	<a href="#help">Special Needs</a> |
	<a href="#release">Releases</a> |
	<a href="#release">Links</a>
	]
</div>

<h3 id="basics">Basic Rules</h3>

Application documents should be stored in a directory under
<code><i>modulename</i>/doc/</code> (where <i>modulename</i> usually is
<tt>kdeedu</tt>) and they should just be the English documents. For example, if
your application is called <i>app</i>, the documents should be in
<code><i>modulename</i>/doc/<i>app</i>/</code>. As many developers may wish to
write their documents in their mother-tongue, it is important that they arrange
to have an initial translation to English. Please contact the
<a href="mailto:kde-edu AT kde DOT org">kde-edu@kde.org</a> mailing list for
assistance in arranging this.

<h3 id="folder_content">What should be in this directory?</h3>

There are certain requirements for KDE documents, and the first one is that
they are written in &quot;docbook XML&quot; format. DocBook is a variant of XML,
and is the format used by all KDE documents. If you were intending to write
plain text instructions, or even HTML, these are sadly not appropriate. If you
have written the document in HTML format, we can probably help you convert it to
DocBook format. The filename for the entire document should be
<tt>index.docbook</tt>. If you use KDevelop, the document template there is
exactly the right format to base your document on.

Please go to [5] to see a list of the tools that can make your writing
easier. Moreover, here[6] are some guidelines to help you get the right
information in your documentation.

If you have images to go in the documents, these should be in the same
directory as your <tt>index.docbook</tt> file. Please don't put them in a
subdirectory.<br />
For guidelines on the size of files you should be using, please see [3].

<h3 id="translation">Translations</h3>

When the documents are ready for translation, let us know, and we can
arrange for them to be automatically passed to the internationalisation (i18n)
team. Once the translations are ready, it is of course possible for application
authors to take them for distribution separately.

<h3 id="mailinglists">Mailing lists</h3>

There are mailing lists for application authors who are writing their own documents. These are:
<ul>
<li><a href="http://mail.kde.org/mailman/listinfo/kde-doc-english/">kde-doc-english</a> (for help on the writing)</li>
<li><a href="http://mail.kde.org/mailman/listinfo/kde-docbook/">kde-docbook</a> (for really overtechnical help with DocBook)</li>
<li><a href="http://mail.kde.org/mailman/listinfo/kde-i18n-doc/">kde-i18n-doc</a> (for both GUI and document translation issues)</li>
</ul>

<h3 id="things-to-remember">Things to remember</h3>

<ul>
<li>the documentation team can (and will) change around the text of documents
  to fit in with KDE rules. For example, they will ensure that Trademarks are
  acknowledged in order to protect the KDE Project from any potential legal
  issues</li>
<li>try to split your <tt>index.docbook</tt> file into different entities</li>
<!--<li>add comments for translators for special instructions if needed</li>-->
<li>make screenshots in the current theme, and following the guidelines [3],
  add screenshots in the same dir than the docbook file(s)</li>
<li>try to find an English native person that will proof-read your doc and
  adjust the terms. You can ask the <a href="http://quality.kde.org/">Quality
  Team</a> for that. If your application is related to a specialized field such
  as Chemistry or Mathematics, try to find someone with the required
  knowledge</li>
<li>Run the English spell-checker</li>
<li>Run <tt>meinproc --check index.docbook</tt> from the console using the
  latest kdelibs to check if your doc is correctly parsed and to check that
  the document is as you want</li>
</ul>

<h3 id="help">Special Needs</h3>

If you have user documents with special needs (for example large clear text,
suitable for small children), please let us know and we will try to help.

<h3 id="release">Releases</h3>

Normally, when a KDE release is due, there will be a two month
&quot;feature freeze&quot;, where no new features should be added to
applications. There is also a shorter &quot;message freeze&quot;, where the
only messages that should be changed should be related to bug-fixes. Even then,
these messages may not end up being translated due to time pressures on the i18n
teams. Please, be sensitive to the pressures on other teams.

<h3 id="links">Links</h3>
<ol>
<li><a href="http://i18n.kde.org/doc/markup/index.html">The KDE DocBook Authors Guide</a>: a quick reference guide to the current KDE DocBook markup standards</li>
<li><a href="http://i18n.kde.org/doc/doc-primer/index.html">The KDE Documentation Primer</a>: how to start writing documentation for KDE</li>
<li><a href="http://i18n.kde.org/doc/screenshots.php">The Screenshot Guidelines</a>: how to take correct screenshots to include in your documentation</li>
<li><a href="http://i18n.kde.org/doc/styleguide/index.html">The KDE style guide</a>: tips and hints on writing correctly</li>
<li><a href="http://i18n.kde.org/doc/tools.php">Tools to improve the writing of documentation</a></li>
<li><a href="http://i18n.kde.org/doc/questionnaire.php">Documentation about hot to improve the contents of your documentation</a></li>
</ol>

<br />
<br />

<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
