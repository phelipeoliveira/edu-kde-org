<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $page_title = i18n_noop( 'Wear KDE Edu' );
  include ( "header.inc" );
?>

<p><?php i18n( 'Here you can find KDE Edu-themed merchandising!')?></p>
<p><a href="http://www.freewear.org/?org=KDEdu"><?php i18n( 'Go for it!')?></a></p>

<?php
  include "footer.inc";
?>
