<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $site_root = "../";
  $page_title = i18n_noop( 'Users Stories' );
  
  include ( "header.inc" );
?>


<h3><?php i18n( '<a href="http://www.voxhumanitatis.org">Vox Humanitatis</a>' ); ?></h3>

<p><?php i18n( '<a href="http://www.voxhumanitatis.org"><img src="../images/icons/vh-pms_100x113.png" style="width:100px;height:113px;float:left;" /></a>Vox Humanitatis (<a href="http://www.voxhumanitatis.org">www.voxhumanitatis.org</a>) cares about less resourced cultures and with that about the connected languages. Since we are also dedicated to the educational sector we are always searching for ways to create educational material which starts from "simple" alphabets to complex grammar exercises and study material for other subjects such as science, mathematics, history etc.' ); ?></p>

<p><?php i18n( "Last autumn we discovered Parley and shortly after that <a href=\"../applications/language/khangman/\">KHangMan</a>. The huge advantage is: you can use contents that are from \"outside\" of the software and even if <a href=\"../applications/language/parley/\">Parley</a> is thought to be a vocabulary trainer it can be used in many, many ways, also for subjects mentioned above. You could call it \"love at first sight\". From there we started to create contents which are too huge to be simply uploaded to <a href=\"http://www.kde-files.org\">kde-files.org</a>. It would take one person's full time work to do it manually and then again: many combinations out of many different data resources will be possible." ); ?></p>

<p><?php i18n( "When meeting in Randa during the <a href=\"http://dot.kde.org/2010/06/19/report-successful-multimedia-and-edu-sprint-randa\">KDE Multimedia and Education sprint 2010</a> the \"how to better collaborate\" was discussed and an export of Agrovoc the FAO agriculture dictionary which is the test-data of Ambaradan, the Vox Humanitatis' data storage engine for dictionaries and similar contents, for <a href=\"../applications/language/parley/\">Parley</a> was created to make sure we were able to provide directly Parley format which allows for an easy download of our data and then to study with it. The second positive point was: Agrovoc right now is not structured, so there are no \"lessons\" and this meant that <a href=\"../applications/language/parley/\">Parley</a> had to digest those 100.000 entries: it did really well..." ); ?></p>

<p><?php i18n( "This means that soon Parley data can be retrieved directly from Ambaradan. In a second stage data for KHangMan and Marble will be made available." ); ?></p>

<p><em><?php i18n( "Sabine Emmy Eller" ); ?></em></p>

<h3><?php i18n( "Further reading:" ); ?></h3>

<ul>
<li><a href="http://www.voxhumanitatis.org/content/when-three-educational-projects-get-together"><?php i18n( 'When three educational projects get together' ); ?></a></li>

<li><a href="http://dot.kde.org/2010/06/19/report-successful-multimedia-and-edu-sprint-randa"><?php i18n( 'Report from Successful Multimedia and Edu Sprint in Randa' ); ?></a></li>

<li><a href="http://www.voxhumanitatis.org/content/kde-education-and-multimedia-sprint-some-results"><?php i18n( 'KDE Education and Multimedia Sprint ... some results ...' ); ?></a></li>

<li><a href="http://www.voxhumanitatis.org/content/editing-parley-its-so-easy-kids-can-do"><?php i18n( "Editing Parley ... it's so easy that kids can do" ); ?></a></li>

<li><a href="http://www.voxhumanitatis.org/content/updated-language-data-online"><?php i18n( 'Updated language data online' ); ?></a></li>

<li><a href="http://www.voxhumanitatis.org/content/joola-fonyi-got-first-game-meaning-first-game-minor-language"><?php i18n( 'Joola Fonyi got the first game - The meaning of the first game for minor language' ); ?></a></li>
</ul>

<?php
  include "footer.inc";
?>
