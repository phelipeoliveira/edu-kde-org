<!DOCTYPE KigMacroFile>
<KigMacroFile Number="6" Version="0.9.0" >
 <Macro>
  <Name>Baricenter</Name>
  <Description>Baricenter of a triangle, given the vertices</Description>
  <IconFileName>gear</IconFileName>
  <Construction>
   <input requirement="point" id="1" />
   <input requirement="point" id="2" />
   <input requirement="point" id="3" />
   <intermediate action="calc" type="SegmentAB" id="4" >
    <arg>3</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="fetch-property" property="mid-point" id="5" >
    <arg>4</arg>
   </intermediate>
   <intermediate action="calc" type="LineAB" id="6" >
    <arg>5</arg>
    <arg>1</arg>
   </intermediate>
   <intermediate action="calc" type="SegmentAB" id="7" >
    <arg>1</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="fetch-property" property="mid-point" id="8" >
    <arg>7</arg>
   </intermediate>
   <intermediate action="calc" type="LineAB" id="9" >
    <arg>8</arg>
    <arg>3</arg>
   </intermediate>
   <result action="calc" type="LineLineIntersection" id="10" >
    <arg>6</arg>
    <arg>9</arg>
   </result>
  </Construction>
 </Macro>
 <Macro>
  <Name>Circumcenter</Name>
  <Description>Circumcenter of a triangle, given the vertices</Description>
  <IconFileName>gear</IconFileName>
  <Construction>
   <input requirement="point" id="1" />
   <input requirement="point" id="2" />
   <input requirement="point" id="3" />
   <intermediate action="calc" type="SegmentAB" id="4" >
    <arg>2</arg>
    <arg>3</arg>
   </intermediate>
   <intermediate action="fetch-property" property="mid-point" id="5" >
    <arg>4</arg>
   </intermediate>
   <intermediate action="calc" type="Copy" id="6" >
    <arg>5</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="7" >
    <arg>4</arg>
    <arg>6</arg>
   </intermediate>
   <intermediate action="calc" type="SegmentAB" id="8" >
    <arg>1</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="fetch-property" property="mid-point" id="9" >
    <arg>8</arg>
   </intermediate>
   <intermediate action="calc" type="Copy" id="10" >
    <arg>9</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="11" >
    <arg>8</arg>
    <arg>10</arg>
   </intermediate>
   <result action="calc" type="LineLineIntersection" id="12" >
    <arg>7</arg>
    <arg>11</arg>
   </result>
  </Construction>
 </Macro>
 <Macro>
  <Name>Gauss Segment</Name>
  <Description>Gauss Segment of a triangle, given the vertices</Description>
  <IconFileName>gear</IconFileName>
  <Construction>
   <input requirement="point" id="1" />
   <input requirement="point" id="2" />
   <input requirement="point" id="3" />
   <intermediate action="calc" type="LineAB" id="4" >
    <arg>1</arg>
    <arg>3</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="5" >
    <arg>4</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="calc" type="LineAB" id="6" >
    <arg>1</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="7" >
    <arg>6</arg>
    <arg>3</arg>
   </intermediate>
   <intermediate action="calc" type="LineLineIntersection" id="8" >
    <arg>5</arg>
    <arg>7</arg>
   </intermediate>
   <intermediate action="calc" type="SegmentAB" id="9" >
    <arg>3</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="fetch-property" property="mid-point" id="10" >
    <arg>9</arg>
   </intermediate>
   <intermediate action="calc" type="Copy" id="11" >
    <arg>10</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="12" >
    <arg>9</arg>
    <arg>11</arg>
   </intermediate>
   <intermediate action="calc" type="SegmentAB" id="13" >
    <arg>1</arg>
    <arg>3</arg>
   </intermediate>
   <intermediate action="fetch-property" property="mid-point" id="14" >
    <arg>13</arg>
   </intermediate>
   <intermediate action="calc" type="Copy" id="15" >
    <arg>14</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="16" >
    <arg>13</arg>
    <arg>15</arg>
   </intermediate>
   <intermediate action="calc" type="LineLineIntersection" id="17" >
    <arg>12</arg>
    <arg>16</arg>
   </intermediate>
   <result action="calc" type="SegmentAB" id="18" >
    <arg>8</arg>
    <arg>17</arg>
   </result>
  </Construction>
 </Macro>
 <Macro>
  <Name>Incenter</Name>
  <Description>Incenter of a triangle, given the vertices</Description>
  <IconFileName>gear</IconFileName>
  <Construction>
    <input requirement="point" id="1" />
    <input requirement="point" id="2" />
    <input requirement="point" id="3" />
    <intermediate action="calc" type="Angle" id="4" >
      <arg>1</arg>
      <arg>3</arg>
      <arg>2</arg>
    </intermediate>
    <intermediate action="fetch-property" property="angle-bisector" id="5" >
      <arg>4</arg>
    </intermediate>
    <intermediate action="calc" type="LineParallel" id="6" >
      <arg>5</arg>
      <arg>3</arg>
    </intermediate>
    <intermediate action="calc" type="Angle" id="7" >
      <arg>2</arg>
      <arg>1</arg>
      <arg>3</arg>
    </intermediate>
    <intermediate action="fetch-property" property="angle-bisector" id="8" >
      <arg>7</arg>
    </intermediate>
    <intermediate action="calc" type="LineParallel" id="9" >
      <arg>8</arg>
      <arg>1</arg>
    </intermediate>
    <result action="calc" type="LineLineIntersection" id="10" >
      <arg>6</arg>
      <arg>9</arg>
    </result>
  </Construction>
</Macro>
 <Macro>
  <Name>Inscribed Circle</Name>
  <Description>Circle inscribed to a triangle, given the vertices</Description>
  <IconFileName>gear</IconFileName>
  <Construction>
    <input requirement="point" id="1" />
    <input requirement="point" id="2" />
    <input requirement="point" id="3" />
    <intermediate action="calc" type="Angle" id="4" >
      <arg>1</arg>
      <arg>3</arg>
      <arg>2</arg>
    </intermediate>
    <intermediate action="fetch-property" property="angle-bisector" id="5" >
      <arg>4</arg>
    </intermediate>
    <intermediate action="calc" type="LineParallel" id="6" >
      <arg>5</arg>
      <arg>3</arg>
    </intermediate>
    <intermediate action="calc" type="Angle" id="7" >
      <arg>2</arg>
      <arg>1</arg>
      <arg>3</arg>
    </intermediate>
    <intermediate action="fetch-property" property="angle-bisector" id="8" >
      <arg>7</arg>
    </intermediate>
    <intermediate action="calc" type="LineParallel" id="9" >
      <arg>8</arg>
      <arg>1</arg>
    </intermediate>
    <intermediate action="calc" type="LineLineIntersection" id="10" >
      <arg>6</arg>
      <arg>9</arg>
    </intermediate>
    <intermediate action="calc" type="SegmentAB" id="11" >
      <arg>1</arg>
      <arg>3</arg>
    </intermediate>
    <intermediate action="calc" type="LinePerpend" id="12" >
      <arg>11</arg>
      <arg>10</arg>
    </intermediate>
    <intermediate action="calc" type="LineLineIntersection" id="13" >
      <arg>11</arg>
      <arg>12</arg>
    </intermediate>
    <result action="calc" type="CircleBCP" id="14" >
      <arg>10</arg>
      <arg>13</arg>
    </result>
  </Construction>
</Macro>
 <Macro>
  <Name>Ortocenter</Name>
  <Description>Ortocenter of a triangle, given the vertices</Description>
  <IconFileName>gear</IconFileName>
  <Construction>
   <input requirement="point" id="1" />
   <input requirement="point" id="2" />
   <input requirement="point" id="3" />
   <intermediate action="calc" type="LineAB" id="4" >
    <arg>1</arg>
    <arg>2</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="5" >
    <arg>4</arg>
    <arg>3</arg>
   </intermediate>
   <intermediate action="calc" type="LineAB" id="6" >
    <arg>1</arg>
    <arg>3</arg>
   </intermediate>
   <intermediate action="calc" type="LinePerpend" id="7" >
    <arg>6</arg>
    <arg>2</arg>
   </intermediate>
   <result action="calc" type="LineLineIntersection" id="8" >
    <arg>5</arg>
    <arg>7</arg>
   </result>
  </Construction>
 </Macro>
</KigMacroFile>
