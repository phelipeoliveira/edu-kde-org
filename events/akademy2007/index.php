<?php
  $site_root = "../";
  $page_title = "KDE-Edu at aKademy 2007";
  

  include( "header.inc" );
?>

<p><b>Education-related events at aKademy 2007 in Glasgow</b></p>
<ol>
  <li><p><b>Inge Wallin: KDE in Large Installations and Thin Client Settings.</b><br />
See the video:<br />
<a href="http://home.kde.org/~akademy07/videos/2-10-KDE_in_Large_Installations_and_Thin_Client_Settings.ogg">
http://home.kde.org/~akademy07/videos/2-10-KDE_in_Large_Installations_and_Thin_Client_Settings.ogg</a><br />
and the page: <br />
<a href="http://akademy2007.kde.org/conference/talks/48.php">http://akademy2007.kde.org/conference/talks/48.php</a></p></li>

<li><p><b>Edu &amp; School Day</b>: 3rd July 2007 <br />
You can find the schedule and slides here:<br />
<a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">http://akademy2007.kde.org/codingmarathon/schoolday.php</a>
and here is the report:<br />
<a href="http://edu.kde.org/events/akademy2007/EduDayReport.odt">http://edu.kde.org/events/akademy2007/EduDayReport.odt</a><br />
(the report has to be improved, if you have improvements you can send the new 
report to <a href="https://mail.kde.org/mailman/listinfo/kde-edu">the kde-edu mailinglist</a> and someone will commit it)</p></li>

<li><p><b>KDe-Edu BoF</b>: Wednesday 4th July.<br />
The reports can be found here:<br />
PDF: <a href="http://edu.kde.org/events/akademy2007/EduBoF.pdf">http://edu.kde.org/events/akademy2007/EduBoF.pdf</a><br />
ODT: <a href="http://edu.kde.org/events/akademy2007/EduBoF.odt">http://edu.kde.org/events/akademy2007/EduBoF.odt</a></p></li>

<li><p><b>Press coverage</b><br /> 
Dot article<br />
<a href="http://dot.kde.org/1183817727/">http://dot.kde.org/1183817727/</a><br />
Mandriva Press release:<br />
<a href="http://www.mandriva.com/en/company/press/pr/mandriva_intel_showcase_the_intel_powered_classmate_pc_at_the_kde_developers_conference">http://www.mandriva.com/en/company/press/pr/mandriva_intel_showcase_the_intel_powered_classmate_pc_at_the_kde_developers_conference</a><br />
LXer:<br />
<a href="http://lxer.com/module/newswire/view/89538/index.html">http://lxer.com/module/newswire/view/89538/index.html</a><br />
Linux.com:<br />
<a href="http://www.linux.com/feed/116901">http://www.linux.com/feed/116901</a><br />
</p></li>

<li><p><b>Blogs</b><br />
Agustin Benito<br />
<a href="http://agustin.ejerciciosresueltos.com/index.php?blog=3&title=meduxa_at_akademy_2007&more=1&c=1&tb=1&pb=1">http://agustin.ejerciciosresueltos.com/index.php?blog=3&title=meduxa_at_akademy_2007&more=1&c=1&tb=1&pb=1</a><br />
<a href="http://agustin.ejerciciosresueltos.com/index.php?blog=3&title=koffice_in_schools&more=1&c=1&tb=1&pb=1">http://agustin.ejerciciosresueltos.com/index.php?blog=3&title=koffice_in_schools&more=1&c=1&tb=1&pb=1</a><br />
<a href="http://agustin.ejerciciosresueltos.com/index.php?blog=3&title=big_deployments_of_desktops_and_kiosk_mo&more=1&c=1&tb=1&pb=1">http://agustin.ejerciciosresueltos.com/index.php?blog=3&title=big_deployments_of_desktops_and_kiosk_mo&more=1&c=1&tb=1&pb=1</a><br />
Jure Repinc<br />
<a href="http://jlp.holodeck1.com/blog/2007/07/03/akademy-2007-school-and-education-day/">http://jlp.holodeck1.com/blog/2007/07/03/akademy-2007-school-and-education-day/</a><br />
<a href="http://jlp.holodeck1.com/blog/2007/07/04/akademy-2007-bofs-day/">http://jlp.holodeck1.com/blog/2007/07/04/akademy-2007-bofs-day/</a><br />
</p></li>
</ol>
<br />
<br />
<?php
  include( "footer.inc" );
?>








