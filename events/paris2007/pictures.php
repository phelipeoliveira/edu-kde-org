<?php
  $site_root = "../";
  $page_title = 'Paris 2007 - Some pictures';
  
  include ( "header.inc" );
?>
<br />

<?php
  $gallery = new EduGallery("KHangMan - Screenshots KDE 3.2rst KDE-Edu Meeting - Some Pictures");
  $gallery->addImage("pics/IMG_7693_sm.png", "pics/IMG_7693.png", 378, 252,  "[Screenshot]", "", "KDE-Edu on Windows by SaroEngels");
  $gallery->addImage("pics/IMG_7699_sm.png", "pics/IMG_7699.png", 378, 251,  "[Screenshot]", "", "KDE-Edu on Windows");
  $gallery->startNewRow();
  $gallery->addImage("pics/IMG_7707_sm.png", "pics/IMG_7707.png", 378, 244,  "[Screenshot]", "", "Vladimir presenting Step");
  $gallery->addImage("pics/IMG_7717_sm.png", "pics/IMG_7717.png", 378, 292,  "[Screenshot]", "", "Anne-Marie: KDE 4.1");
  $gallery->startNewRow();
  $gallery->addImage("pics/IMG_7723_sm.png", "pics/IMG_7723.png", 378, 317,  "[Screenshot]", "", "Our Group");
  $gallery->show();
?>
<p>Thanks to Aliona Kuznetsova for these pictures!</p>
 <br />
 <hr width="30%" align="center" />
 <p>
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>














