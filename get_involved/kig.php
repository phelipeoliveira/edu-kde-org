<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $page_title = i18n_noop( "Writing Kig Macros" );
  include "header.inc"; 
?>

<p><?php i18n( "You can extend Kig with macros written in Python. Please look at the <a href=\"/kig/manual/scripting-api/\">Kig Python Scripting API Documentation</a>." ); ?></p>

<p><?php i18n ("There are some macros available in the <a href=\"../contrib/kig/\">Contribution section</a>." ); ?></p>


<?php include "footer.inc"; ?>
