<?php
  $page_title = "Translating";
  $site_root = "../";
  
  include( "header.inc" );
  $appinfo = new AppInfo( "KTurtle" );
  $appinfo->setVersion( "0.6" );
  $appinfo->setCopyright( "2003", "Cies Breijs" );
  $appinfo->setLicense("gpl");
  $appinfo->addContributor( "Cies Breijs", "cies # showroommama DOT nl", "Dutch data files" );
  $appinfo->addContributor( "Anne-Marie Mahfouf", "annma AT kde DOT org", "French data files" );
  $appinfo->addContributor( "Matthias Me&szlig;mer", "bmlmessmer AT web DOT de", "German data files" );
  $appinfo->addContributor( "Burkhard L&uuml;ck", "lueck AT hube-lueck DOT de", "German data files" );
  $appinfo->addContributor( "Stefan Asserh&#228;ll", "stefan DOT asserhall AT telia DOT com", "Swedish data files" );
  $appinfo->addContributor( "Jure Repinc", "jlp AT holodeck1 DOT com", "Slovenian data files" );
  $appinfo->addContributor( "Chusslove Illich", "caslav.ilic AT gmx DOT net", "Serbian (Cyrillic and Latin) data files" );
  $appinfo->addContributor( "Pino Toscano", "toscano DOT pino AT tiscali DOT it", "Italian data files" );
  $appinfo->addContributor( "Andy Potter", "A DOT J DOT Potter AT rhul DOT ac DOT uk", "English GB data files" );
  $appinfo->addContributor( "Rafael Beccar", "rafael DOT beccar AT kdemail DOT net", "Spanish data files" );
  $appinfo->addContributor( "Riverson Rios", "riverson AT ccv DOT ufc DOT br", "Brazilian Portuguese data files" );
  $appinfo->addContributor( "Karl Ove Hufthammer", "karl AT huftis DOT org", "Norwegian Bokm&aring;l and Nynorsk data files" );
  $appinfo->addContributor( "pbs", "pbs AT linux DOT net DOT pl", "Polish data files" );
  $appinfo->addContributor( "Marián Výboh", "kturtle AT centrum DOT sk", "Slovak data files" );
  $appinfo->show();
?>


<br />
<br />

<h3><a name="current">Translating KTurtle for KDE 4</a></h3>
		<p>Follow <a href="translator.php">this link</a> for information about translations for KTurtle in trunk.</p>

<br />
<br />

<h3><a name="kde3">Translating KTurtle in KDE 3.x</a></h3>

<blockquote>
<b>NOTE:</b>
There is a lot of work lately on totally new release of KTurtle, the way this new release (that will be part of KDE4) will handle translations will change completely.
Translating KTurtle will then be much easier! Please take in account that the work you put in translating KTurtle in KDE3 will <i>not</i> be usable for the KDE4 release of KTurtle. You might want to consider translating KTurtle at a later point in time when KDE4 is (pre-)released (I guess some prerelease should be out in the beginning of 2007).<br/>
Thank you for your understanding.
</blockquote>

<p>
As you know, the Logo language great specificity is that the Logo commands can be translated in your own language. This makes it easier for a child to understand the commands.<br />
Already supported languages are (code): de_DE, en_US, en_GB, es, fr_FR, it, nb, nl, nn, pl, pt_BR, sl, sr, sr@Ltn, sv.<br />
For a new language, there are 3 sort of files to translate: first the keywords file (or commands) then the logo-highlight-style file and finally the examples. You send them to <a href="mailto:annemarie.mahfouf AT free.fr">Anne-Marie Mahfouf</a> who will add them in the kde-i18n module after checking if everything works well.
</p>
<p>
You can look on the internet with Google to search for similar translation (in Google type: Logo programming language_name). This could help you. Otherwise, just think about  words that would make the more sense for kids.<br />
</p>
<p>The ISO code for your language can be found <a href="http://i18n.kde.org/teams/index.php?sort=isocode">here</a>.
</p>
<h4>1 - Translating the Logo Commands for KTurtle</h4>
<p>The commands files are located in <tt>kdeedu/kturtle/src/data/keywords/</tt><br />
Each file is named<br />
<tt>logokeywords.code.xml</tt><br />
where <i>code</i> is the iso code for your country (note that this must be a valid iso code).</p>
<ol>
<li>Copy the logokeywords.en_US.xml file by renaming it to logokeywords.code.xml where code is your country code (2 or 4 letters ISO code)
</li>
<li>Translate in your own language the content of the &lt;keyword&gt; tag i.e the information between &lt;keyword&gt; and &lt;/keyword&gt; whenever it's possible and the content of the &lt;alias&gt; tag, i.e the information between the &lt;alias&gt; and &lt;/alias&gt;. These contents are related as the alias is a shortcut for the keyword.<br />

For example translate 'while' in: &lt;keyword&gt;while&lt;/keyword&gt;.<br />

Please don't translate anything else and don't translate English words in &lt;command name="english_word"&gt;. These must stay in English.
</li>
<li>Save your file as <b>utf-8</b> (in Kate, use Save As and change to utf8 in the box on the right of the file name)</li>

<li>Send the file to Anne-Marie.</li>

<li>In case of any doubt, please contact <a href="mailto:annemarie.mahfouf AT free.fr">Anne-Marie Mahfouf</a> for more information.</li>
</ol>

<h4>2 - Translating the the syntax highlighting files for KTurtle</h4>
<p>The syntax highlighting files are located in kdeedu/kturtle/src/data/highlightstyles/<br />
Each file is named<br />
<tt>logohighlightstyle.code.xml</tt><br />
where <i>code</i> is the ISO code for your country (note that this must be a valid iso code).<br />
Please note that the country code must be the same here than in the logokeywords file and for the example folder.</p>
<ol>
<li> Copy the logohighlightstyle.en_US.xml file by renaming it to logohighlightstyle.code.xml where code is your country code (2 or 4 letters ISO code)</li>
<li>Line 4 of the file, there is<br />
<tt>&lt;language name="en_US" </tt><br />
change en_US with your language ISO code (2 or 4 letters).</li>
<li>Translate in your own language the content of the &lt;item&gt; tag i.e the information between &lt;item&gt; and &lt;/item&gt;. This content must match the logokeyword file.<br />
For example translate 'while' in: &lt;item&gt; while &lt;/item&gt; and leave the spaces as they are (one at the beginning and one at the end).<br />
Please don't translate anything else.</li>
<li>Save your file as <b>utf-8</b> (in Kate, use Save As and change to utf8 in the box on the right of the file name)</li>
<li>Send the file to Anne-Marie.</li>
<li>Please don't forget to translate the examples, see below.</li>
<li>In case of any doubt, please contact <a href="mailto:annemarie.mahfouf AT free.fr">Anne-Marie Mahfouf</a> for more information.</li>
</ol>

<h4>3 - Translating the Logo Examples for KTurtle</h4>
<p>The examples are located in the KTurtle source in the <tt>kdeedu/kturtle/examples</tt> folder.
</p>
<ol>
<li>Create a folder with your language iso as name</li>
<li>Copy the english example in this folder and rename the files with your language translation. This will allow users to easily and quickly understand what the example is about.</li>
<li>See in kturtle/src/data/keywords/ the file corresponding to your language. This file must be done before translating the examples. 
Replace in the example files all English keywords with your translated keywords.<br />
Save your file as <b>utf-8</b> (in Kate, use Save As and change to utf8 in the box on the right of the file name)</li>
<li>Send the folder tar.gz to Anne-Marie.</li>
<li>In case of any doubt, please contact <a href="mailto:annemarie.mahfouf AT free.fr">Anne-Marie Mahfouf</a> for more information.</li>
<li>Finally, if you want, you can add your own examples in this folder.</li>
</ol>
<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


