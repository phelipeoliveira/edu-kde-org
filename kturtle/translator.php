<?php
  $page_title = "Notes to Translators of KTurtle";
  $site_root = "../";
  
  include( "header.inc" );
  $appinfo = new AppInfo( "KTurtle" );
  $appinfo->setVersion( "0.8" );
  $appinfo->setCopyright( "2003", "Cies Breijs" );
  $appinfo->setLicense("gpl");
  // please don't add contributers, it clutters up the page... thank you.
  $appinfo->show();
?>


<br />
<blockquote>
<b>NOTE:</b>
This document describes how to translate KTurtle with a version >= 0.8, otherwise known as the version that comes with KDE4. For translating the older KDE3 versions of KTurtle please see <a href="./translation.php">this page</a>, yet keep in mind that the translation work you do on a KDE3 version of KTurtle will not work for the KDE4 version.
</blockquote>
<br />

<h2><a name="current">Translating KTurtle</a></h2>
<p>
KTurtle is an educational programming environment for kids. The most appealing feature of KTurtle is that the programming language used is translated to the native language of the programmer. So the programming commands, the programming examples, the error messages, the GUI, the manual... everything is translated. This way the programmer can program in his/her native language, and this enables one to learn how to program before understanding English!
</p>
<p>
This means translations are very important to KTurtle... so <i>you</i> are very important to KTurtle!<br />
    Before you start translating KTurtle I invite you to take a short look at KTurtle. It is part of <a href="http://edu.kde.org">kde-edu</a>. Once you started KTurtle you can open an example (<i>File -> Examples</i>) and Run (<i>File -> Run</i>) it. In the <i>Settings</i> menu you find a language chooser, there you can select the script language of the KTurtle programming environment. An other nice feature is the context help you get if you hit F2; just place your cursor on a programming command and hit F2.
</p>

<br />
<br />

<h2><a name="kde3">How did you get here?</a></h3>
<p>
You probably got to this page by following a link in a POT-file generated from the KTurtle source. Good!<br >
Next you will find the instructions for the 3 important cases of strings that need a special treatment:
</p>

<br />
<h3><a name="commands">First case: the COMMANDS</a></h3>
<p>
As mentioned earier in this document: the programming language used in KTurtle is translated to the native language of the programmer... Translating the programming language itself is what you do by translating strings marked as a COMMAND.<br />
To translate the commands please keep the following things in mind:
<ul>
	<li>You are translating for possibly very young people: stick to very basic language.</li>
	<li>COMMANDS can only contain <i>letters</i> and <i>underscores</i>, so no spaces, no numbers, no special charaters like punctuation, etc.</li>
	<li>Try to keep it consistent! If some commands have a similar meaning, they should have similar look. So <b>turnleft</b> and <b>turnright</b> should not become <b>turnleft</b> and <b>aimright</b>.</li>
	<li>Consistency (again). If you, for instance, decide to use underscores in the translations of your commands then use them consistently for all commands.</li>
	<li>Commands must be <i>unique</i>, or the program will crash!</li>
	<li>Be careful with the <i>ArgumentSeparator</i> (a comma in en_US), and the <i>DecimalSeparator</i> (a dot in en_US). In your language the comma might be used as <i>DecimalSeparator</i>, in that case you can use a semicolon '<b>;</b>' as <i>ArgumentSeparator</i> -- but you don't have to! If you think it is acceptable to use the dot as <i>DecimalSeparator</i> please do so: in programming languages the dot is usually used as <i>DecimalSeparator</i> it might be good to learn this right from the start.
	<li>Choose wise, choose to last: people will learn these commands to make little programs, so only change something if you have a good reason to do so. The KTurtle programming language (TurtleScript) is loosely based on the 'Logo' programming language. Logo is also often translated to the programmers native language; you can search the internet for Logo translations in your language -- in Google type: 'Logo programming language_name' -- this could help you a bit.</li>
</ul>
</p>

<br />
<h3><a name="commands">Second case: the COMMAND ALIASES</a></h3>
<p>
In KTurtle some commands have an alias. This alias is <i>just an abbreviation</i> of the command. This is because some commands of the very small command set are used very often, so in order to have quicker results one could use the aliases for these often used commands.<br />
For instance the <b>forward</b>, <b>backward</b> and the <b>turnleft</b>, <b>turnright</b> commands. They are used very often, so in en_US they are aliased to respectively: <b>fw</b>, <b>bw</b> and <b>tl</b>, <b>tr</b>.<br />
Some things to keep in mind when you translate command aliases:
<ul>
	<li>Just like commands, aliases can only contain <i>letters</i> and <i>underscores</i>. But please avoid the use of <i>underscores</i> in aliases.</li>
	<li>Try to keep it consistent! Try to choose the same letter for the same part of different COMMAND. For instance commands starting with <b>turn</b> (like: <b>turnright</b>) always have aliases that start with <b>t</b> (like: <b>tr</b>).</li>
	<li>Aliases must be <i>unique</i>, and cannot be the same as a COMMAND, or the program will crash!</li>
</ul>
</p>

<br />
<h3><a name="example_names">Third case: the EXAMPLE NAMES</a></h3>
<p>
The example names are the names of the examples you find in <i>File -> Examples</i>. Please translate them into something simple, yet appealing for kids.<br />
You should probably start KTurtle and open (<i>File -> Examples</i>) and Run (<i>File -> Run</i>) all examples once, to know what they look like, so you have a better idea of what you are translating the name of.<br />
The translation of the code in these examples is generated by KTurtle at runtime from the translated commands.
</p>


<br />

<h2><a name="testing">Testing</a></h2>
<p>
After you finished translating KTurtle please have a look if everything worked. A good way to do this is to execute all examples, by <i>File -> Examples</i> and then <i>File -> Run</i>. If an example gives strange results you can switch the language to English (en_US) in <i>Settings -> Script Language</i> and see if it produces different results, if so please <a href="http://bugs.kde.org">file a bug</a> report. If the examples run fine you are quite sure you have succeeded.
</p>
<p>
Thanks for translating KTurtle!
</p>

<br />
<br />

<h2><a name="documentation">Documentation</a></h2>
<p>
Translating the strings is a big deal for KTurtle. But to make the program really shine is to translate the documentation as well. This is because KTurtle has a built-in command help funtion: put your cursor on a 'word' (command, number, variable, string, etc) in the editor and press <b>F2</b>... You will be presented with help on this particular topic!
<blockquote>
<b>NOTE:</b>
At the moment of writing this 'F2' feature is not yet fully implemented.
</blockquote>
</p>

<br />
<br />

<h2><a name="question">Questions?</a></h2>
<p>
It is quite likely that not every thing is clear, or that there are mistakes, or that for your language you need more configurability, or that it just does not. Please let us know!<br />
You can always file a bug report at <a href="http://bugs.kde.org">bugs.kde.org</a> against the version of KTurtle you are translating, we will definitely have a look at it. With your feedback we will be able to improve the translation system and KTurtle in general.
</p>
<p>
You can also seek contact directly with the core developers/ maintainers of KTurtle. Cies Breijs's email is: <i>cies -at- kde -dot- nl</i>, and Mauricio Piacentini can be reached at <i>piacentini at kde dot org</i>.
</p>

<br />
<br />

<hr width="30%" align="center" />
<p>Author: Cies Breijs<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


