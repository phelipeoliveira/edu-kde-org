<?php
  $page_title = "Ideas for icons";
  $site_root = "../";

  include( "header.inc" );
?>

<h3>H</h3> 
<li>rocketfuel </li>
<li>hardening fat </li>
<li>desulfuring oil (remove S from oil)</li>
<li>synthesis of ammonium</li>
<li>cryogenic, luminous paints (Tritium)</li>

<h3>He</h3>
<li>Zeppelins, Balloons</li>
<li>Laser</li>
<li>Cooling, for example nuclear power plants - Refrigerant used in cooling</li>
<li>for scuba-diving, arc-welding</li>

<h3>Li</h3>
<li>Addition to oil for motors etc</li>
<li>Space-alloy</li>
<li>high quality glass (laboratory-tools) </li>
<li>used in Batteries</li>
<li>desiccant (hydroxide), in mobile phones (see Ta), air cleaning in spacecrafts (reacts with CO<sub>2</sub> to O<sub>2</sub>), in red firework</li>

<h3>Be</h3>
<li>The small parts in clocks and watches</li>
<li>High quality tools (where there must never be sparks) - manufacture of light corrosion-resistant alloys</li>
<li>Windows of xray-tubes</li>
<li>gyroscopes</li>

<h3>B</h3>
<li>Moderator in nuclear power plants - retards neutrons</li>
<li>- Tennis rackets</li>
<li>High-temerature glass</li>
<li>Bleaching</li>
<li>green firework, washing powder, fishing rods, wood protection</li>

<h3>C</h3>
<li>Diamonts - Diamonds</li>
<li>Pencils</li>
<li>Moderator in nuclear power plants</li>
<li>Plastics</li>
<li>Wood</li>
<li>a fern, fullerene (soccer ball), a chimney sweep. Activated charcoal to absorb toxins and poisons in medicine, <sup>14</sup>C for organic matter dating in archeology</li>

<h3>N</h3>
<li>Cooling</li>
<li>synthesis of ammonium</li>
<li>Fertiliser - Fertilizer</li>
<li>explosives, beer dispensers, laughing gas</li>

<h3>O</h3>
<li>Fire</li>
<li>Steel production</li>
<li>Sand</li>
<li>Water</li>
<li>an oxygen mask. Used for welding, oxydation.</li>

<h3>F</h3>
<li>Uran-235 synthesis (UF<sub>6</sub> is used)</li>
<li>Teflon</li>
<li>Toothpaste</li>
<li>Cooling</li>
<li>a fridge. Used for cooling in fridges and air-conditioning.</li>

<h3>Ne</h3>
<li>Neon-lamps</li>
<li>Spots</li>
<li>Laser</li>

<h3>Na</h3>
<li>Steetlamps (the yellow on, for example in BeNeLux)</li>
<li>Cooling in nuclear power plants</li>
<li>Batteries</li>
<li>common salt, Soda, glass</li>
<li>soap. Desiccant in water-free solvants.</li>

<h3>Mg</h3>
<li>Underwater fire</li>
<li>Motorparts</li>
<li>Camera flash</li>
<li>anti-slipping powder on hands (talc used in gymnastics, climbing)</li>
<li>used in muscle conductivity (against cramps in the legs for example), extensively used for its laxative proprieties.</li>

<h3>Al</h3>
<li>Windows</li>
<li>Tubes</li>
<li>Foild -Foil</li>
<li>Cable</li>
<li>Automobils</li>
<li>Rockets</li>
<li>mirror</li>
<li>Main metallic compound in the following gemstones: topaz, corundum (ruby and sapphire), chrysoberyl, spinel, turquoise.</li>

<h3>Si</h3>
<li>Electrical circuts</li>
<li>Silicon-oil</li>
<li>Tools</li>
<li>Glass (Glass is SiO<sub>2</sub>)</li>
<li>Sand (sand is also SiO<sub>2</sub>)</li>
<li>computer chip, concrete, bricks, solar cells. </li>
<li>Pottery, enamel.  Main compound (other than H and O) in the following gemstones: quartz, opal, beryl (aquamarine, emerald, heliodor).</li>

<h3>P</h3>
<li>Firework</li>
<li>Fertelizer -Fertiliser</li>
<li>Toothpaste</li>
<li>disinfectant</li>
<li>desiccant, In bones and proteins.  </li>

<h3>S</h3>
<li>Matches</li>
<li>Batteries</li>
<li>Rubber-production</li>
<li>Hair-tightener (if you want to have curly hair)</li>
<li>rotten eggs (H<sub>2</sub>S is the stuff which smells so strongly)</li>
<li>car battery, car tyres (vulcanized rubber).</li>
<li>Fungicide, fumigant, conservative for dried fruits and wine</li>

<h3>Cl</h3>
<li>Bleaching</li>
<li>HCl (acid)</li>
<li>Water purification (swimmingpool)</li>
<li>PVC (plasitc, poly vinyle chlorine)</li>
<li>Anti-rust</li>

<h3>Ar</h3>
<li>bulbs</li>
<li>Laser</li>
<li>Geiger-counter (for radioactivity)</li>
<li>Protective gas (i18n("Schutzgasschweißen"))</li>

<h3>K</h3>
<li>Fertalizer -Fertiliser</li>
<li>optical glass (glasses)</li>
<li>Matches</li>
<li>Black powder</li>

<h3>Ca</h3>
<li>Many metals need it in production</li>
<li>Cable insulation</li>
<li>Fertalizer - Fertiliser</li>
<li>Concrete</li>
<li>bones</li>
<li>Stalactites and stalagmites in caves.</li>
<li>Cement, mortar, concrete. Plaster to fix broken bones.</li>

<h3>Sc (scandium)</h3>
<li>in alloys used for bycicle-frames and lacrosse sticks. High intensity lights (added to mercury-vapor lamps)</li>

<h3>Ti (titanium) </h3>
<li>frames for glasses, medical instruments, hip or knee prothesis, heat protection shield of spacecrafts</li>

<h3>V (vanadium) </h3>
<li>with chromium and iron alloy for tools (resists corrosion), magnets</li>

<h3>Cr (chromium) </h3>
<li>to harden steel in iron alloys (see above), leather tanning</li>
<li>yellow paints. Is part of jewels (emerald and ruby)</li>
<li>surface protection against corrosion</li>

<h3>Mn (manganese) </h3>
<li>various alloys, oxidation agent (permanganate). In amethyst.  Important enzymes in eukaryotic (bacteria and animal) metabolism to prevent toxic effect of oxygen.</li>

<h3>Fe (iron) </h3>
<li>tools, cars, etc. Blood (a vampire?)</li>


<h3>Co (cobalt) </h3>
<li>blue paint, jet engine, radiotherapy against cancer, vitamine B12, food-irradiation</li>

<h3>Ni (nickel)</h3>
<li> small coins, in batteries, in many corrosion-resistant alloys</li>

<h3>Cu (copper)</h3>
<li>copper coloured coins, wires</li>

<h3>Zn, Zinc</h3>
<li>brass instruments, batteries, white paint</li>

<h3>Ga</h3>
<li>Silicate-thermometer</li>
<li>information saving (hdd and such)</li>
<li>transistor</li>
<li>localisation of tumors (cancer)</li>
<li>blue LED's, neutrino detection, replaces mercury in thermometers</li>

<h3>Ge</h3>
<li>Infrared prisma - infrared prism </li>
<li>transidor - Transistor</li>
<li>wide-angle objectives (photographie) - photography</li>
<li>in night-vision glasses, thermographic cameras, semiconductor</li>

<h3>As</h3>
<li>shotgun-bullets (in 'lead shots')</li>
<li>The topmost mirror-surface (a mirror would be a good symbol)</li>
<li>Diodes</li>
<li>Glass</li>
<li>Laser</li>
<li>pyrotechnics and firework, wafer-coating</li>

<h3>Se</h3>
<li>Xeroxmachine</li>
<li>Solarpanel</li>
<li>Anti-dandruff shampoo</li>

<h3>Br</h3>
<li>Teargas</li>
<li>Fireprotection</li>
<li>Film (photo film)</li>
<li>anti-germ stuff (in hospitals and so on) - disinfectant</li>
<li>is not used anymore as a disinfectant because of its toxicity.  It has been totally replaced by organic Iodine compounds.</li>

<h3>Kr</h3>
<li>fluorescent tube </li>
<li>Flash-bulbs (for photos and such)</li>
<li>Wavelength standard</li>
<li>UV-Laser</li>
<li>Superman, as many kids can associate it to "kriptonite" :-)</li>

<h3>Rb</h3>
<li>Fotocells (solar pannel) - Photocells</li>
<li>Heartmuscle-science</li>
<li>Vakuum-gastrap - Vacuum</li>
<li>atomic clock in satelites</li>

<h3>Sr</h3>
<li>Nuclearbatteries</li>
<li>beta-rays are created with it</li>
<li>bright colours</li>
<li>Fireworks</li>
<li>the red "phosphor" in CRT colour screens</li>

<h3>Y (yttrium)</h3>
<li>red colour in TV-tubes, microwaves (a microwave oven?)</li>

<h3>Zr (zirconium)</h3>
<li>"fake" diamonds, in nuclear reactors, bicycle-frames</li>

<h3>Nb (niobium)</h3>
<li>antiallergic jewellery, pacemaker, in particle accelerators (CERN)</li>

<h3>Mo (molybdenum)</h3>
<li>high temperature lubricant, in alloys for space- and aircraft parts, high-strength steel alloys, red and orange paints</li>

<h3>Tc (technetium)</h3>
<li>radioactive tracer in medical test for various organs because of it's short half-life (6 hours)</li>

<h3>Ru, Ruthenium</h3>
<li>hardener for platinum, palladium and titan, Parker 51 fountain pen has a ruthenium-hardened nib.</li>

<h3>Rh, Rhodium</h3>
<li>used for electrodes, jewellery (protects gold and platinum against corrosion), car catalytic converters, catalyst in many chemical reactions</li>

<h3>Pd, Palladium</h3>
<li>jewellery, crowns in dentistry, in watches</li>
<li>surgical instruments, to make white gold, in car catalytic converters (replaces the more expensive platinum)</li>

<h3>Ag, Silver</h3>
<li>Spoons</li>
<li>jewellery, mirror, money</li>

<h3>Cd, Cadmium</h3>
<li>nuclear fission barrier, in batteries</li>

<h3>In</h3>
<li>Solarpanel</li>
<li>Mirrorsurface</li>
<li>Fotocells (CCD) - Photocell</li>
<li>Transistor</li>
<li>Blood- and lung science</li>

<h3>Sn</h3>
<li>Emaille (that is that old porcelain-like stuff) </li>
<li>Coins</li>
<li>Organ-pipe</li>
<li>pewter plates or other tableware, tin cans, solder (solded circuits?)</li>

<h3>Sb</h3>
<li>Lead-batteries</li>
<li>infrared detectors</li>
<li>anti-germ medicine (like peniciline)</li>
<li>solder, red matches, fire-protection</li>
<li>regarding the "anti-germ" use: actually it has no use against bacterias but as an anti-parasitic drug, mainly against shistosoma, a flatworm parasite and leishmania, a flagelate worm.</li>

<h3>Te</h3>
<li>For vulcanisation of rubber</li>
<li>electrical resistors</li>
<li>optical fiber (as chalcogenic glass), has higher refractions than SiO<sub>2</sub></li>

<h3>I (Iodine)</h3>
<li>Desinfection (medicine) - Disinfectant</li>
<li>Halogenlamps</li>
<li>Ink-particles</li>
<li>Addition to common salt (for cooking)</li>
<li>seaweed (smell)</li>

<h3>Xe (Xenon)</h3>
<li>UV-Lamp</li>
<li>Solarium</li>
<li>Projectors (those used in classrooms for example)</li>
<li>Car lights, film projectors, stroboscopes, anaesthesia</li>

<h3>Cs</h3>
<li>Fotocells</li>
<li>gamma-rays</li>
<li>atomic clocks</li>
<li>Infrared-lamps</li>

<h3>Ba</h3>
<li>Sparkplug</li>
<li>Vakuum-gastrap</li>
<li>Flourencencelamps</li>
<li>X-ray diagnosis of the gastro-intestinal tube, green firework</li>
<li>benitoite gem stone (blue), high-refraction glass</li>

<h3>La (lanthanum)</h3>
<li>flintstone in lighters (mischmetal), in NiMeH batteries</li>

<h3>Ce (cerium)</h3>
<li>in mischmetal together with lanthanum or as ferrocerium (flintstone), catalyst in "self-cleaning" oven alloys, glass polishing</li>

<h3>Pr (praseodymium</h3>
<li>in mischmetal as flintstone, carbon arc lights</li>

<h3>Nd (neodymium)</h3>
<li>strong magnets in microphones, loudspeakers, harddisks, volcano eruption prediction, high energy lasers</li>

<h3>Pr (promethium)</h3>
<li>luminous paint. Gauges to mesure thickness and/ord filling levels.</li>

<h3>Sm (samarium)</h3>
<li>Carbon arc lights, neutron-absorber, quartz watches, harddisks, headphones</li>

<h3>Eu (europium</h3>
<li>red phosphor in fluorescent lamps and CRT monitors, in euro-banknotes as an anti-counterfeiting phosphor.</li>

<h3>Gd (gadolinium)</h3>
<li>in CD and RAM manufacturing. Organic compounds of Gd are injected during magnetic resonance imaging as a paramagnetic agent. Could replace F compounds in fridges and airconditioning.</li>

<h3>Tb (terbium)</h3>
<li>doping materials in solid-state devices, crystal stabilisator in high-temperature fuel cells</li>

<h3>Dy (dysprosium)</h3>
<li>CD manufacturing</li>

<h3>Ho (holmium)</h3>
<li>yellow glass colouring</li>

<h3>Er (erbium</h3>
<li>pink glass and enamel colouring</li>

<h3>Tm (thulium)</h3>
<li>in dosimeters for low-radiation detection</li>

<h3>Yb (ytterbium)</h3>
<li>Radiation source for portable X-ray devices, as a difluorite compound in high quality dentistry filings which allows a permanent fluoride protection.</li>

<h3>Lu (lutetium)</h3>
<li>szintigraphic crystal for PET-scanning</li>

<h3>Hf (hafnium)</h3>
<li>Flash lamps in photography, HfO<sub>2</sub> will replace SiO<sub>2</sub> in wafers (announcements made by IBM and Intel in January 2007)</li>

<h3>Ta (tantalum)</h3>
<li>High melting point alloys, surgical instruments, cranial repair plates, microelectronic devices (in mobilephones), on cameralenses</li>

<h3>W (tungsten)</h3>
<li>Darts (the game), electric lamp filament, electron tubes, high melting point alloys, kinetic energy penetrators (anti-tank ammunition), glass-to-metal seals, violin strings, jewellery, etc.</li>

<h3>Re (rhenium)</h3>
<li>car catalysts together with platinum, filaments in mass spectrography, fountain pen tips</li>

<h3>Os (osmium)</h3>
<li>very hard alloys, fountain pen tips, OsO<sub>4</sub> was used to take fingerprints before graphite, phonograph needles, pacemakers (platinum alloy)</li>

<h3>Ir (iridium)</h3>
<li>Sparkplugs, crucibles for high temperature use, polymer LED, the platinum-iridium alloy is used for standard weights</li>

<h3>Pt (platinum)</h3>
<li>jewellery, wire, bridges in dentistry, catalytic converters in cars, crucibles for glass melting</li>

<h3>Au (gold)</h3>
<li>A gold medal, a gold bar, a crown, etc.</li>
<li>Use: anti-oxidant thin-layer coating of many materials (good  infra-red reflector for satelites, electric connectors), jewellery, decoration, in dentistry</li>

<h3>Hg, mercury</h3>
<li>a barometer, a thermometer, calomel electrode, switches</li>
<li>Builds alloys with nearly every other metal, in batteries. Cheap rotating parabolic mirrors.</li>

<h3>Tl</h3>
<li>Thermometer</li>
<li>Infrared-detector</li>
<li>Anti-insect-spray</li>
<li>Heartmuscle-science</li>
<li>rat-poison (old), photo cells, glass lenses in photocopy and fax machines</li>

<h3>Pb</h3>
<li>XRay-protection</li>
<li>Batteries</li>
<li>Colours/Paintings</li>
<li>Batteries, ammunition, radiation shields, lead glass, ballast in ships, scubadiving waist belts</li>

<h3>Bi</h3>
<li>Catalyses something when producing rubber</li>
<li>Safety (electrical circut)</li>
<li>Glas - Glass</li>
<li>ceramics </li>
<li>Stomach disinfection</li>
<li>fire detection devices. replaces lead in many ways because of its low toxicity</li>

<h3>Po</h3>
<li>nuclear batteries</li>
<li>Neutrons-source</li>
<li>Antistatic stuff</li>
<li>Filmcleaner</li>

<h3>At (astatine)</h3>
<li>No use as it is radioactive</li>
<li>For treatment of thyroid cancers as it can replace Iodine in the thyroid. Extremly rare on earth.</li>

<h3>Rn</h3>
<li>Earthquake prediction</li>
<li>In hydrology to examine ground water connection to streams and rivers</li>

<h3>Fr</h3>
<li>extremly rare, not handable in solid state as it develops far to much heat capacity.</li>

<h3>Ra, Radium</h3>
<li>Neutron source</li>
<li>Medical therapy with rays</li>
<li>self luminous paints, formerly used in watches to make indicators visible in the dark.</li>

<h3>Ac (actinium)</h3>
<li>far to radioactive (more than Plutonium)</li>

<h3>Th (thorium)</h3>
<li>on optic lenses to raise refraction index. For uranium233 production.</li>

<h3>Pa (protactinium)</h3>
<li>no application because of its rarity and radioactivity</li>

<h3>U (uranium)</h3>
<li>main use as fuel in nuclear power plants.Icon could be a cooling tower. High density penetrators for military use.</li>

<h3>Np (neptunium</h3>
<li>to produce plutonium in breeders.</li>

<h3>Pu (plutonium)</h3>
<li>batteries for spacecrafts (Cassini probe). Nuclear weapons. </li>

<h3>Am (americium)</h3>
<li>smoke detectors</li>

<h3>Cm (curium)</h3>
<li>alpha-source for the alpha-source X-ray-spectrometer (ASXS) used for chemical analysis on various Mars mission robots.</li>

<h3>Bk (berkelium)</h3>
<li>no use</li>

<h3>Cf (californium)</h3>
<li>Eventually as a portable neutron source. Has been used to produce element no, 118: ununoctium</li>


<p>Author: Carsten Niehaus<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include("footer.inc");
?>
