<?php
  $page_title = "Kalzium";
  $site_root = "../";
  

  include( "header.inc" );

  $appinfo = new AppInfo( "Kalzium" );
  $appinfo->setIcon( "../images/icons/kalzium_32.png", "32", "32" );
  $appinfo->setVersion( "1.4.6" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2001", "by the Kalzium Team" );
  $appinfo->addAuthor( "Carsten Niehaus", "cniehaus AT kde DOT org" );
  $appinfo->addMaintainer( "Etienne Rebetez", "etienne DOT rebetez AT oberwallis DOT ch" );
  $appinfo->addContributor( "Pino Toscano", "toscano.pino AT tiscali DOT org", "Major code contributions" );
  $appinfo->addContributor( "Martin Pfeiffer", "hubipete AT gmx DOT net", "Major code contributions" );
  $appinfo->addContributor( "Chris Luetchford", "chris AT os11 DOT com", "svg icon" );
  $appinfo->addContributor( "Thomas Nagy", "tnagy2^8 AT yahoo DOT fr", "Contributed the code of <a href=\"http://edu.kde.org/eqchem/\">EqChem</a>" );
  $appinfo->addContributor( "Marcus Hanwell", "", "SoC on the molecular viewer and libavogadro porting/integration");
  $appinfo->show();
?>

<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">News</a> |
  <a href="#join">Join us!</a> |
  <a href="#developerblogs">Developers blogs</a> |
  <a href="#news">News</a>
]
</div>

<h3><a name="description">Description</a></h3>
<p>
  Kalzium is an application which will show you some information about the
  periodic system of the elements. Therefore you could use it as an information
  database.
</p>
<p>
  Kalzium has already some nice features but there are still a lot of things
  to do. As Kalzium has been included in KDE 3.1 we are currently coding
  quite lot to bring in some new and nice features and make it as fast
  and usefull as possible. You can help us by testing it or make proposals
  for new features or a better design.
</p>

<h3><a name="join">Join Us!</a></h3>

<p>
Interested in helping develop Kalzium? We need programmers, of course,
but even if you don't code, we can still use your help. If you are an
astronomy enthusiast, you could help us with improving the <a
href="glossary.php">Glossary</a> project. Of course, the more data 
Kalzium has, the better. So if you have information Kalzium could make
use of please share them with us.</p>
<p>We could also use help
writing documentation and translating. Even bug reports and feature
requests are a big help, so keep them coming!</p>

<h3><a name="developerblogs">Developers blogs</a></h3>
<p><a href="http://www.livejournal.com/users/cniehaus/">Carsten Niehaus</a></p>

<?php
  kde_general_news("./news.rdf", 10, true);
?>

<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include("footer.inc");
?>

