<table border="0" cellpadding="5" cellspacing="10">

<tr>
<th colspan="2" class="contentheader">Languages</th>
<th class="contentheader" style="text-align:center">Since KDE</th>
</tr>

<tr>
<td valign="top"><a href="../kanagram/index.php"><img alt="Kanagram" align="top" src="../images/icons/kanagram_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kanagram/index.php">Kanagram</a> is an anagram game.</td>
<td style="text-align:center">3.5</td>
</tr>

<tr>
<td valign="top"><a href="../khangman/index.php"><img alt="KHangMan" align="top" src="../images/icons/khangman_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../khangman/index.php">KHangMan</a> is the classical hangman game.</td>
<td style="text-align:center">3.0</td>
</tr>

<tr>
<td valign="top"><a href="../kiten/index.php"><img alt="Kiten" align="top" src="../images/icons/kiten_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kiten/index.php">Kiten</a> is a Japanese reference/learning tool.</td>
<td style="text-align:center">3.1</td>
</tr>

<tr>
<td valign="top"><a href="../klettres/index.php"><img alt="KLettres" align="top" src="../images/icons/klettres_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../klettres/index.php">KLettres</a> aims to help to learn the alphabet and then to read some syllables in different languages.</td>
<td style="text-align:center">3.0</td>
</tr>

<tr>
<td valign="top"><a href="../kwordquiz/index.php"><img alt="KWordQuiz" align="top" src="../images/icons/kwordquiz_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../kwordquiz/index.php">KWordQuiz</a> is a tool that gives you a powerful way to master new vocabularies. It replaces FlashKard since KDE 3.3.</td>
<td style="text-align:center">3.3</td>
</tr>

<tr>
<td valign="top"><a href="../parley/index.php"><img alt="Parley" align="top" src="../images/icons/parley_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../parley/index.php">Parley</a> is a vocabulary trainer. It has some pretty advanced features and is nice to use. It is the successor of KVocTrain.</td>
<td style="text-align:center">4.0</td>
</tr>

<tr><td><br/></td><td><br/></td></tr>

<tr>
<th colspan="2" class="contentheader">Discontinued in KDE 4.0</th>
<th class="contentheader" style="text-align:center">Since KDE</th>
</tr>

<tr>
<td valign="top"><a href="../klatin/index.php"><img alt="KLatin" align="top" src="../images/icons/klatin_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../klatin/index.php">KLatin</a> was dropped in KDE 4.0 as it had no maintainer anymore.</td>
<td style="text-align:center"> - </td>
</tr>

<tr>
<td valign="top"><a href="../kvoctrain/index.php"><img alt="KVocTrain" align="top" src="../images/icons/kvoctrain_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kvoctrain/index.php">KVoctrain</a> became Parley for KDE 4.0.</td>
<td style="text-align:center"> - </td>
</tr>

</table>
