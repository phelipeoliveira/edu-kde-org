<?php
	$page_title = "KmPlot";
	
	include( "header.inc" );
	
	$appinfo = new AppInfo( "KmPlot" );
	$appinfo->setIcon( "../images/icons/kmplot_32.png", "32", "32" );
	$appinfo->setVersion( "1.2.0" );
	$appinfo->setLicense("gpl");
	$appinfo->setCopyright( "2001", "Klaus-Dieter M&ouml;ller" );
	$appinfo->addAuthor( "Klaus-Dieter M&ouml;ller", "kdmoeller AT foni DOT net" );
	$appinfo->addContributor( "Matthias Me&szlig;mer", "bmlmessmer AT web DOT de", "CVS, coding, docs and web-site" );
	$appinfo->addContributor( "Frederik Edemar", "f_edemar AT linux DOT se", "coding, new features" );
	$appinfo->addContributor( "Robert Gogolok", "robertgogolok AT web DOT de", "porting to KDE 3" );
	$appinfo->addContributor( "David Vignoni", "david80v AT tin DOT it", "svg icon" );
	$appinfo->addContributor( "David Saxton", "david AT bluehaze DOT org", "Porting to Qt 4, UI improvements, features" );
	$appinfo->show();
?>

<h3><a name="description">Description</a></h3>

<p>KmPlot is a mathematical function plotter for the KDE-Desktop. </p>
<p>It has built in a powerfull parser. You can plot different functions simultaneously and combine their function terms to build new functions.
KmPlot supports functions with parameters and functions in polar coordinates. Several grid modes are possible. Plots may be printed with high precision in correct scale.
</p>

<h4>Features</h4>
<ul>
	<li>powerful mathematical parser</li>
	<li>precise metric printing</li>
	<li>different plot types (functions, parametric, polar)</li>
	<li>highly configurable visual settings (plot line, axes, grid)</li>
	<li>export to bitmap format (BMP and PNG) and scalable vector graphics (SVG)</li>
	<li>save/load complete session in readable xml format</li>
	<li>trace mode: cross hair following plot, coordinates shown in the status bar</li>
	<li>support zooming</li>
	<li>ability to draw the 1st and 2nd derivative and the integral of a plot function</li>
	<li>support user defined constants and parameter values</li>
	<li>various tools for plot functions: find minium/maximum point, get y-value and draw the area between the function and the y-axis</li>
</ul>

<h3>More Information</h3>

<ul>
	<li><a href="screenshots.php">Some screenshots...</a></li>
	<li><a href="todo.php">What needs to be improved in KmPlot...</a></li>
	<li><a href="obtain.php">How to obtain KmPlot...</a></li>
</ul>

<?php
	include( "footer.inc" );
?>
